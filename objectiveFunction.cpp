#include <iostream>
#include <iomanip>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#include "objectiveFunction.h"



objectiveFunction::objectiveFunction(double** scenariosMatrix, int lengthW, int nScenarios, int timeHorizonScenarios, double energy0, double deltaCharge, double deltaDischarge, double capacity) {
	this->scenariosMatrix = scenariosMatrix;
	this->lengthW = lengthW;
	this->nScenarios = nScenarios;
	this->timeHorizonScenarios = timeHorizonScenarios;
	this->energy0 = timeHorizonScenarios;
	this->deltaCharge = deltaCharge;
	this->deltaDischarge = deltaDischarge;
	this->capacity= capacity;
}


double  objectiveFunction::compute(double* weights){
	double payoffTot = 0.0;
	double payoff = 0.0;
	double probability = 1.0 / nScenarios; // probabilit� di uno scenario
	double energy = energy0;
	if (lengthW <=2 ){
		cout << "lunghezza dei pesi non corretta" << endl;
		return 0.0;
	}

	for (int s = 0; s < nScenarios; s++) {
		// lunghezza di lengthW � la lunghezza del vettore, uno indica il peso dell'energia, uno vuole la costante
		// quindi mi serve 
		for (int i = lengthW - 3; i < timeHorizonScenarios; i++) {
			// se c'� energia da prendere allora mi chiedo se la prendo
			if (scenariosMatrix[0][i] > 0) {
				double ris = 0.0;
				for (int j = 0; j < lengthW - 3; j++) {
					ris += weights[j] * scenariosMatrix[0][i - j];
				}
				ris += weights[lengthW - 2] * energy + weights[lengthW - 1];
				if (ris > 0) {
					double delta = min(deltaCharge, capacity - energy);
					energy += delta;
					scenariosMatrix[0][i] -= delta;
				}
			}// altrimenti c'� richiesta non soddisfatta
			else {
				// se riesco a colmare la richiesta allora:
				if (scenariosMatrix[0][i] + min(energy, deltaDischarge) > 0) {
					energy += scenariosMatrix[0][i];
				}// altrimenti black out:
				else {
					energy = 0;
					payoff = 2*capacity;
				}
			}
		}
		payoff += energy;
		payoffTot += payoff*probability;
	}

	return payoffTot;
}