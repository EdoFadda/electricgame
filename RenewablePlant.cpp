// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#include "eg.h"
#include "RenewablePlant.h"
#include <fstream>

// methods to interact with the world
double RenewablePlant::Ask(systemState s, bool forced){
	return 0.0;
}

double RenewablePlant::Bid(systemState s, bool forced){
	return 0.0;
}

void RenewablePlant::Transfer(systemState s, double amount) {
	historyProduction.push_back(amount);
}

void RenewablePlant::getForecast(double** scenariosMatrix, int nScenarios, int timeHorizonForecast){
	LOG << "ATTENZIONE: getForecast per il renewablePlant non funziona ancora";
	for (int i = 0; i < nScenarios; i++) {
		for (int t = 0; t < timeHorizonForecast; t++) {
			scenariosMatrix[i][t] += 0.0;
		}
	}
}


// plot
void RenewablePlant::DumpHtml(string path){
	ostringstream oss;
	oss << path << "/renewablePlant" << Id << ".html";
	ofstream fileO(oss.str().c_str());


	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;

	// drawHistoryAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'production ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < historyProduction.size(); i++) {
		fileO << "      [" << i << "," << -historyProduction[i]<< "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div id=\"BidAsk\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}


bool RenewablePlant::isStorage() {
	return false;
}

