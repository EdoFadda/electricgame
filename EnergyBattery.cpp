// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "EnergyBattery.h"

using namespace std;


# define CHARGE_CODE  1
# define DISCHARGE_CODE  -1




// methods to interact with the world
double EnergyBattery::Ask(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf > 0) {
		ask = bf;
		bid = 0.0;
		return bf;
	}
	else {
		return 0.0;
	}

}

double EnergyBattery::Bid(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if (Brain(forced) < 0) {
		ask = 0.0;
		bid = -bf;
		return -bf;
	}
	else {
		return 0.0;
	}
}


void EnergyBattery::Transfer(systemState s, double amount) {
	// amount negativo -> ceduto energia
	// amount positivo -> acqistato energia
	if (ResidualLife == 0) {
		EnergyBatteryState bS = { ResidualLife, Energy, 0.0, 0.0 ,0.0 ,0.0, 0.0, Payoff };
		BatteryHistory.push_back(bS);
		return;
	}


	Energy = min(Energy + amount, Capacity);
	SystemStates.push_back(s);

	// Arrange life
	
	if (BatteryHistory.size() == 0 ){
		ResidualLife--;
	}else{
		//if (sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) ) // se non hanno lo stesso segno allora
		//	ResidualLife--;
		if (amount > 0 && sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert)) {
			//ResidualLife -=  tanh(Energy) + 1.0;
			ResidualLife--;
		}
	}
	
	// update payoff
	this->Payoff +=  max(ResidualLife, 0.0);


	// update BatteryHistory
	EnergyBatteryState bS = {ResidualLife, Energy, amount, ask ,bid ,amount, 0.0, Payoff};
	BatteryHistory.push_back(bS);
	
	presentLife++;
}


// output methods
double EnergyBattery::getEnergy() {
	return Energy;
}

double EnergyBattery::getEnergyThreshold(){
	return this->Threshold;
}

double EnergyBattery::getDeltaCharge(){
	return this->DeltaCharge;
}

double EnergyBattery::getResidualLife(){
	return this->ResidualLife;
}



// internal intelligence
double EnergyBattery::Brain(bool forced) {
	if (forced) {
		return -min(DeltaDischarge,Energy);
	}else{
		if(Energy < Threshold) { // se ho meno energia del threshold allora continuo a caricare
			return min(DeltaCharge, Capacity - Energy);
		}else{// se ho più energia del threshold allora 
			if (BatteryHistory.size() == 0) {
				return min(DeltaCharge, Capacity - Energy);
			}else{
				if (sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) == CHARGE_CODE) {
					return min(DeltaCharge, Capacity - Energy);
				}
			}
		}
	
	}
}


void EnergyBattery::newThreshold(int version, bool verbose, int nVite){ // se version=DETEVO il prossimo thereshold sarà scelto in base alla storia, 
															 // se version=STOCEVO il prossimo thereshold sarà scelto a caso.
	// we have finish a set of lives:
	if (bestStat.ntimeStep == -1) {
		if (verbose) { 
			LOG <<"Aggiorno perche' e' il primo giro, il nuovo BestEnergyThreshold e': " << Threshold << "  n Time Step: " << this->presentStat.ntimeStep;
			LOG << ""; LOG << "";
		}
		this->BestEnergyThreshold = Threshold;
		this->bestStat.ntimeStep = this->presentStat.ntimeStep;
		this->bestStat.sumPayoff = this->presentStat.sumPayoff;
		this->bestStat.sumSquarePayoff = this->presentStat.sumSquarePayoff;
		presentStat = { 0,0.0,0.0 };
		return;
	}


	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;
	double variancePayOff = presentStat.sumSquarePayoff / presentStat.ntimeStep - pow(meanPayOff, 2.0);
	if (verbose) { LOG << "PayOff: " << meanPayOff << " variance: " << variancePayOff << " n: " << presentStat.ntimeStep; }

	double meanBestPayOff = bestStat.sumPayoff / bestStat.ntimeStep;
	double varianceBestPayOff = bestStat.sumSquarePayoff / bestStat.ntimeStep - pow(meanBestPayOff, 2.0);
	if (verbose) { LOG << "Best PayOff: " << meanBestPayOff << " variance Best: " << varianceBestPayOff << " n: " << bestStat.ntimeStep; }

	
	double s = varianceBestPayOff/ bestStat.ntimeStep + variancePayOff / presentStat.ntimeStep;
	
	double z = (meanPayOff- meanBestPayOff) / pow(s,0.5);


	if (verbose) { LOG << " z test: " << z <<" Quantile Thereshol"<< QuantileThreshold; }

	// se il test mi dice che i nuovi sono migliori allora aggiorno il passato con il presente: 
	if (z > QuantileThreshold) {
		if (verbose) { LOG << "aggiorno perche' z: " << z << " maggiore di threshold: " << QuantileThreshold; }
		if (verbose) { LOG << "quindi il nuovo BestEnergyThreshold e': " << Threshold; }
		this->BestEnergyThreshold = Threshold;
		this->bestStat.ntimeStep = this->presentStat.ntimeStep;
		this->bestStat.sumPayoff = this->presentStat.sumPayoff;
		this->bestStat.sumSquarePayoff = this->presentStat.sumSquarePayoff;
	}
	if (verbose) { LOG << "Best threshold: " << BestEnergyThreshold; }
	
	if (version == DETEVO) {
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		if (verbose) { LOG << "minEnergy: " << minEnergy; }
		if (minEnergy <= 0) { // se il minimo dell'energia che ho è 0 allora aumento il threshold
			if (Threshold >= Capacity) {
				Threshold = BestEnergyThreshold;
			}
			Threshold = Threshold + varianceChangeThreshold*abs(d(gen));
		}else { // altrimenti lo diminuisco
			Threshold = Threshold - minEnergy;
		}
		Threshold = min(Capacity, Threshold);
	}else if (version == STOCEVO) {
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		Threshold = Threshold + varianceChangeThreshold*d(gen); 
		Threshold = min(Capacity, Threshold);
	}
	else if (version == SIMULATEDANEALING) {
		if (nVite == 0){
			LOG << "nVite per la modalità SIMULATEDANEALING deve essere diverso da zero";
			exit(1);
		}
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		Threshold = BestEnergyThreshold + Capacity*exp(-5.0*presentLife/nVite)*d(gen); // in questo modo quando presentLife == nVite uso una varianza del 6 per mille della capacità
		presentLife ++ ;
		Threshold = ((int)Threshold) % ((int)Capacity);
	}else {
		LOG << "version non corretta";
		exit(1);
	}
	
	if (verbose) { LOG << "new Threshold: " << Threshold; LOG << ""; LOG << ""; }

	// e inizializzo le nuove statistiche:
	presentStat = { 0,0.0,0.0 };

}

void EnergyBattery::setVarianceChangeThreshold(double value){
	this->varianceChangeThreshold = value;
}


void EnergyBattery::printThreshold() {
	LOG << this->bestStat.sumPayoff / this->bestStat.ntimeStep << "  " << this->BestEnergyThreshold;
}


// startSimulation ed endSimulation aggiornano a inizio e fine di ogni Run()
void EnergyBattery::startSimulation(){
	this->Payoff = 0.0;
	this->Energy = BatteryHistory[0].Energy;
	this->ResidualLife = BatteryHistory[0].ResidualLife;
	while (BatteryHistory.size() != 0) {
		BatteryHistory.erase(BatteryHistory.begin());
	}
	while (SystemStates.size() != 0) {
		SystemStates.erase(SystemStates.begin());
	}
	BatteryHistory.push_back({ this->ResidualLife, this->Energy, 0.0,0.0,0.0,0.0,0.0 });
}

void EnergyBattery::endSimulation() {
	// aggiorno minEnergy:
	for (int i = 0; i < SystemStates.size(); i++) {
		if (BatteryHistory[i].Energy < minEnergy)
			minEnergy = BatteryHistory[i].Energy;
	}

	this->presentStat.ntimeStep++;
	this->presentStat.sumPayoff += this->Payoff;
	this->presentStat.sumSquarePayoff += pow(this->Payoff, 2.0);
}



bool  EnergyBattery::isStorage() {
	return true;
}

// plot
void EnergyBattery::DumpHtml(string path) {
	ostringstream oss;
	oss << path << "/battery" << Id << ".html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	// chiamo le funzioni per visualizzare i grafici
	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;
	fileO << "google.setOnLoadCallback(drawResidualLife);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawBidAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Bid << "," << BatteryHistory[i].Ask << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawResidualLife
	fileO << "function drawResidualLife() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'residualLife');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].ResidualLife << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ResidualLife'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;
	fileO << "		<div id=\"ResidualLife\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}


void EnergyBattery::printStatistics() {
	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;

	double variancePayOff = presentStat.sumSquarePayoff / presentStat.ntimeStep - pow(meanPayOff, 2.0);
	
	LOG<<"Threshold:"<<Threshold << "PayOff: " << meanPayOff << " variance: " << variancePayOff << " n: " << presentStat.ntimeStep;
}

void EnergyBattery::changeThresholdTo(double newThreshold) {
	//BatteryHistory.empty();
	this->Payoff = 0.0;
	this->Threshold = newThreshold;
	presentStat = { 0,0.0,0.0 };
	this->bestStat = { -1, 0.0, 0.0 };
}

double EnergyBattery::getMeanPayOff() {
	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;
	return meanPayOff;
}