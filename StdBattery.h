// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_BATTERYSTD_H
#define EG_BATTERYSTD_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<vector>
#include "string"
#include "Player.h"
#include "Battery.h"



class StdBattery : public Battery {
public:

	StdBattery(double cap, double energy0, int life ) : Battery(cap, energy0, life) {
		this->Energy = energy0;
		this->Capacity = cap;
		this->ResidualLife = life;
		HistoryEnergy.push_back(energy0);
	}
	
	bool isStorage();

	double Ask(systemState s, bool forced = false);
	double Bid(systemState s, bool forced = false);
    
	void Transfer(systemState s, double amount);
	void DumpHtml(string path);
	void endSimulation();
	void startSimulation() { return; };
	double getEnergy();

	double Brain(bool forced);

private:
	int ResidualLife;
	double Energy;
    double Load;
    double Capacity;
	std::vector<double> HistoryEnergy;
	std::vector<double> HistoryAsk;
	std::vector<double> HistoryBid;
	std::vector<double> HistoryPayOffs;
};

#endif //EG_BATTERY_H
