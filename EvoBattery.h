// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_EVOBATTERY_H
#define EG_EVOBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include "string"
#include "Player.h"
#include "Battery.h"



class EvoBattery : public Battery {
public:

	EvoBattery(double cap, double energy0, int life, double lower = -1.0, double upper = 1.0) : Battery(cap, energy0, life) {
		this->Energy = energy0;
		this->Capacity = cap;
		this->ResidualLife = life;
		this->Payoff = 0.0;
		if (lower == 0) {
			cout << "### Plese return a non zero lower threshold"<<endl;
			exit(1);
		}
		if (upper == 0) {
			cout<< "### Plese return a non zero upper threshold"<<endl;
			exit(1);
		}
		this->Lower = lower;
		this->Upper = upper;
		this->presentLUStat = {0,0.0,0.0};
		this->bestLUStat = {100,-40000.0, 16000000.0 }; // suppongo di avere 100 volte -400
	}
	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);
    
	void Transfer(systemState s, double amount);
	

	void newLU(); // comando da usare quando finisco un set di vite

	void endSimulation();
	void startSimulation() { return; }; // ToDo: Implement

	void printBounds();

	void DumpHtml(string path);
	void print();

	double getEnergy();
	double Brain(bool forced);
	double Predictor();
	

private:
	// technical data:
	int ResidualLife;
	double Energy;
    double Load;
    double Capacity;
	
	// decisional data LONG
	double Lower;
	double Upper;

	double BestLower;
	double BestUpper;

	double threshold = -1.64; // In realtà dovrebbe dipendere dai gradi di libertà qt(0.05, 1000)


	// decision data short SHORT
	double ask;
	double bid;

	// statistics of past lives(with different coefficients):
	statistics bestLUStat;
	
	// statistics of lives:
	statistics presentLUStat;

	

	// HistoricalData:
	std::vector<batteryState> BatteryHistory; // qua merizzo la storia della Batteria nella sua vita corrente
	std::vector<systemState>  SystemStates; 

	
};





#endif //EG_EVOBATTERY_H
