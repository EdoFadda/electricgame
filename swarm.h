#ifndef SWARM1_H
#define SWARM1_H

/*
Sources:
Kennedy, J. and Eberhart, R. C. Particle swarm optimization.
       Proc. IEEE int'l conf. on neural networks Vol. IV, pp. 1942-1948.
       IEEE service center, Piscataway, NJ, 1995.
PSO Tutorial found at: http://www.swarmintelligence.org/tutorials.php
*/
#include <iostream>
#include <iomanip>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <cmath>


using namespace std;

class cParticle;
class objectiveFunction;

class swarm{

	cParticle* particles; // swarm
	int nEpochs;
	int inputDim;           // Number of variables to be optimized
	int nParticles;
	float V_MAX;           //Maximum velocity change allowed.
	
	cParticle* bestParticle;

	/*The particles will be initialized with data randomly chosen within the range of these starting min and max values: */
	double START_RANGE_MIN;
	double START_RANGE_MAX;
	double globalBest = DBL_MAX;

	public:
		swarm(int inputDim, int nParticles,  int nEpochs,float V_MAX, double* RANGE_MIN, double* RANGE_MAX, objectiveFunction* ObjFunc, double omega, double phi_ind, double phi_glb);
		void returnBestPosition(double* ris);
		void psoAlgorithm(bool verbose=false);
		void updateParticles(int gBestIndex);
		int  minimum();
		void printBest();
};

#endif