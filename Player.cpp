// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#include "Player.h"
#include "eg.h"

using namespace std;

double Player::GetPayoff() {
    return this->Payoff;
}

void Player::Punishment(){
	this->Payoff = this->Payoff-0.2*abs(this->Payoff + 1.0);
}



std::string Player::Tag() {
    return fmt::format("Player({0:p})", reinterpret_cast<void *>(this));
}

