// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "EvoBattery.h"

using namespace std;



// methods to interact with the world
double EvoBattery::Ask(systemState s, bool forced){
	if (ResidualLife == 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if ( bf > 0) {
		ask = bf;
		bid = 0.0;
		return bf;
	}else {
		return 0.0;
	}

}

double EvoBattery::Bid(systemState s, bool forced){
	if (ResidualLife == 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if (Brain(forced) < 0) {
		ask = 0.0;
		bid = bf;
		return -bf;
	}else {
		return 0.0;
	}
}

void EvoBattery::Transfer(systemState s, double amount) {
	
	Energy += amount;
	
	SystemStates.push_back(s);

	// Arrange life
	if (amount != 0) {
		ResidualLife--;
	}

	// update payoff
	if (ask > 0) {
		this->Payoff += 2*amount-ask;
	}else if (bid > 0) {
		this->Payoff += 2*bid+amount;// amount is negative
	}
		
	
	// update BatteryHistory
	if (amount >= 0) {
		batteryState bS = {Energy, ask ,bid ,amount, 0.0, Payoff };
		BatteryHistory.push_back(bS);
	}else {
		batteryState bS = { Energy, ask ,bid , amount, 0.0, Payoff };
		BatteryHistory.push_back(bS);
	}

}


// output methods
double EvoBattery::getEnergy(){
	return Energy;
}

// internal intelligence
double EvoBattery::Brain(bool forced) {
	if (forced) {
		return -0.5*Energy;
	}else{
		double prediction = Predictor();
		if (prediction > Upper) {
			return prediction - Upper;
		}
		if(prediction < Lower) {
			return prediction - Lower;
		}
		return 0.0;
	}
}

double EvoBattery::Predictor() {
	int memory = 2;
	if (SystemStates.size() == 0) {
		return 0.0;
	}
	double ris = 0.0;
	if (SystemStates.size() <= memory) {
		for (int i = 0; i < SystemStates.size(); i++) {
			ris += (SystemStates[i].totBid - SystemStates[i].totAsk) / SystemStates.size();
		}
		return ris;
	}

	for (int i = SystemStates.size()-1; i > SystemStates.size() - 1-memory; i--) {
		ris += (SystemStates[i].totBid - SystemStates[i].totAsk) / memory;
	}
	return ris;
}


void EvoBattery::newLU() {
	// we have finish a set of lives:
	bool reject = true;

	double meanPayOff = presentLUStat.sumPayoff / presentLUStat.ntimeStep;
	double variancePayOff = presentLUStat.sumSquarePayoff / presentLUStat.ntimeStep - pow(meanPayOff, 2.0);
	//LOG << "PayOff: " << meanPayOff << " variance: " << variancePayOff<< " n: "<< presentLUStat.ntimeStep;

	double meanBestPayOff = bestLUStat.sumPayoff / bestLUStat.ntimeStep;
	double varianceBestPayOff = bestLUStat.sumSquarePayoff / bestLUStat.ntimeStep - pow(meanBestPayOff, 2.0);
	//LOG << "Best PayOff: " << meanBestPayOff << " variance Best: " << varianceBestPayOff<<" n: " << bestLUStat.ntimeStep;

	//LOG << "BestPayOff: " << meanBestPayOff << " variance: " << varianceBestPayOff;
	//bestLUStat.ntimeStep = 8;
	//double varianceBestPayOff = pow(36.0,2.0);
	//double meanBestPayOff = 1237;

	//double meanPayOff = 1036;
	//double variancePayOff = pow(40.0,2.0);
	//presentLUStat.ntimeStep = 7;

	double s = (bestLUStat.ntimeStep - 1)*varianceBestPayOff + (presentLUStat.ntimeStep - 1)*variancePayOff;
	s= s/(bestLUStat.ntimeStep + presentLUStat.ntimeStep-2.0);
	s = pow(s* (1.0 / bestLUStat.ntimeStep + 1.0 / presentLUStat.ntimeStep), 0.5);
	double t = (meanBestPayOff - meanPayOff) / s;


	// se il test mi dice che i nuovi sono migliori allora aggiorno il passato con il presente: 
	if (t < threshold) {
		//LOG << "aggiorno perche' t: " << t << " minore di threshold: "<<threshold;
		//LOG << "quindi sono meglio lower:" << Lower << " Upper: " << Upper;
		this->BestLower = Lower;
		this->BestUpper = Upper;
		this->bestLUStat.ntimeStep = this->presentLUStat.ntimeStep;
		this->bestLUStat.sumPayoff = this->presentLUStat.sumPayoff;
		this->bestLUStat.sumSquarePayoff = this->presentLUStat.sumSquarePayoff;
	}
	//LOG << "Best lower:" << BestLower << " bestUpper: " << BestUpper;
	// cambio di una quantità casuale Simulated Anealing Stye
	double nVal1 = BestLower + BestLower*((double)rand() / (double)RAND_MAX);
	double nVal2 = BestUpper + BestUpper*((double)rand() / (double)RAND_MAX);


	// aggiorno i valori
	this->Lower = min(nVal1, nVal2);
	this->Upper = max(nVal1, nVal2);
	//LOG << "new Lower: " << Lower << " new Upper: " << Upper;

	// e inizializzo le nuove statistiche:
	presentLUStat = {0,0.0,0.0};

}


void EvoBattery::printBounds() {
	LOG<< this->bestLUStat.sumPayoff/this->bestLUStat.ntimeStep <<"  "<<this->BestLower << " " << this->BestUpper;
}


void EvoBattery::endSimulation() {
	this->presentLUStat.ntimeStep++;
	this->presentLUStat.sumPayoff += this->Payoff;
	this->presentLUStat.sumSquarePayoff += pow(this->Payoff, 2.0);
	this->Payoff = 0.0;
	while (BatteryHistory.size() != 0) {
		BatteryHistory.erase(BatteryHistory.begin());
	}
	while (SystemStates.size() != 0) {
		SystemStates.erase(SystemStates.begin());
	}
}

bool  EvoBattery::isStorage() {
	return true;
}

// plot
void EvoBattery::DumpHtml(string path){
	ostringstream oss;
	oss << path << "/battery" << Id << ".html";
	
	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;
	// drawEnergy
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Bid << "," << BatteryHistory[i].Ask << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

void EvoBattery::print(){
	LOG << "Energy: " << this->Energy << "\t Predictor: " << Predictor();
}

