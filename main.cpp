// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#include "eg.h"
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <amp_graphics.h>

#include "Simulator.h"
#include "Battery.h"
#include "Building.h"
#include "NuclearPlant.h"
#include "EvoBattery.h"
#include "EnergyBattery.h"
#include "WiseBattery.h"
#include "StdBattery.h"
#include "AdaptiveBattery.h"
#include "TvBattery.h"


using namespace std;

std::shared_ptr<spdlog::logger> _eg;

int main() {

	
	_eg = spdlog::stdout_logger_mt("console");
	spdlog::set_pattern("[%H:%M:%S.%e] %v");

	LOG << "ElectricGames v0.1";
	LOG << "(!) spring 2016 by Edoardo Fadda & Giovanni Squillero";

/*CONVERGENZA DUE BATTERIE*/
	//double capacity = 1000.0;
	//double energy0 = 150.0;
	//int life = 1e+5;
	//double deltaCharge = 50.0;
	//double deltaDischarge = capacity;
	//double threshold = 0.0;
	//double variance = 100.0;
	//for (int i = 0; i < 10; i++) {
	//	vector<Player *> players;
	//	players.push_back(new NuclearPlant(100.0));
	//	AdaptiveBattery* b1 = new AdaptiveBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 24);
	//	AdaptiveBattery* b2 = new AdaptiveBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 7);
	//	players.push_back(b1);
	//	players.push_back(b2);
	//	players.push_back(new Building(24.0, 150.0, variance));
	//	Simulator s{ players };
	//	//s.Run(100);
	//	s.Run(24 * 300);
	//	b1->DumpDat("C:/Users/Edoardo/Desktop", 1);
	//	b1->DumpDat("C:/Users/Edoardo/Desktop", 2);
	//}



/*TABELLA CONFRONTO*/
//	ofstream fileO("C:/Users/utente/Desktop/tab.txt");//FISSO CASA
//	//ofstream fileO("C:/Users/edofa/Desktop/tab.txt");// LAPTOP
//	//ofstream fileO("C:/Users/Edoardo/Desktop/tab_new.txt");// LAPTOP
//	fileO << "Type" << "\t" <<"life"<< "\t" << "blackout" << endl;
//
//	double capacity = 1000.0;
//	double energy0 = 0.5;
//	int life = 1e+5;
//	double deltaCharge = capacity/2;
//	double deltaDischarge = capacity;
//	double threshold = 0.0;
//	double variance = 0.0; // 50.0;
//	double amplitude = 100;// 150.0;
//	int Nrip = 1;
//	int Ndays = 100; // 30;
//	double s1 = 0.0;
//	double v1 = 0.0;
//	double b1 = 0;
//	double t1 = 0.0;
//	int i = 0;
//	for (i = 0; i < Nrip; i++) {
//		vector<Player*> players;
//		players.push_back(new NuclearPlant(100.0));
//		TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 1, 120,0.05, 1000);
//		players.push_back(b);
//		Building* casa = new Building(24.0, amplitude, variance);
//		casa->setPeriodicSwitch(24 * Ndays / 6, 200, 50);
//		players.push_back(casa);
//		Simulator s{ players };
//		//s.setSwitch(24 * Ndays / 4, 200, 50);
//		s.Run(24 * Ndays);
//		s1 += (life-b->getResidualLife())/life;
//		v1 += pow((life - b->getResidualLife()) / life, 2.0);
//		if(s.happensBlackOut()) b1++;
//		// LAPTOP
//		//b->DumpDat("C:/Users/edofa/Desktop/CARTELLA",i);
//		//s.dumpResult("C:/Users/edofa/Desktop");
//		// PC UFFICIO
//		//b->DumpDat("C:/Users/Edoardo/Desktop",i);
//		//s.dumpResult("C:/Users/Edoardo/Desktop");
//		//PC CASA
//		//b->DumpDat("C:/Users/utente/Desktop",i);
//		s.dumpResult("C:/Users/utente/Desktop");
//		LOG<<"AD_" << i<<" "<< b->getThreshold()<<" ("<<b->getStdDev()<<") "<< (life - b->getResidualLife()) / life <<" "<< s.happensBlackOut()<<" "<<b->getMinThreshold();
//	}
//	s1 = s1 / ((double)Nrip);
//	v1 = v1 / ((double)Nrip) - pow(s1, 2.0);
//	b1 = b1 / ((double)Nrip);
//	fileO << "Adaptive \t" << s1 << "(" << sqrt(v1) << ")" << " " << b1<<endl;
//	LOG << "Adaptive \t" << s1 << "(" << sqrt(v1) << ")" << " " << b1;
//
//	
//// 0
//	for (int j = 0; j <= 20; j++) {
//		double s_0 = 0.0;
//		double v_0 = 0.0;
//		double b_0 = 0.0;
//		for (i = 0; i < Nrip; i++) {
//			vector<Player*> players;
//			players.push_back(new NuclearPlant(100.0));
//			EnergyBattery*b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 2, ((double)j)/20.0*capacity);
//			players.push_back(b);
//			Building* casa = new Building(24.0, amplitude, variance);
//			casa->setPeriodicSwitch(24 * Ndays / 6, 200, 50);
//			players.push_back(casa);
//			Simulator s{ players };
//			//s.setSwitch(24 * Ndays / 4, 200, 50);
//			s.Run(24 * Ndays);
//			s_0 += (life-b->getResidualLife())/life;
//			v_0 += pow((life-b->getResidualLife())/life, 2.0);
//			if (s.happensBlackOut()) b_0++;
//			//LOG << "0_" << i;
//		}
//		s_0 = s_0 / ((double)Nrip);
//		v_0 = v_0 / ((double)Nrip) - pow(s_0, 2.0);
//		b_0 = b_0 / ((double)Nrip);
//		fileO << ((double)j) / 20.0 << "\t" << s_0 << "(" << sqrt(v_0) << ")" << " " << b_0 << endl;
//		LOG << ((double)j) / 20.0 << "\t" << s_0 << "(" << sqrt(v_0) << ")" << " " << b_0;
//	}
//	std::cout << '\a';

	///*GRAFICO THRESHOLD VS VARIANCE*/
	//ofstream fileO("C:/Users/utente/Desktop/ThresholVariance_Grezzi.dat"); // FISSO  
	//ofstream fileO("C:/Users/edofa/Desktop/ThresholVariance.dat"); // PORTATILE
	ofstream fileO("D:/Dropbox/BackUp Lavori/Squillero/ThresholVariance.dat"); // UFFICIO
	fileO << "variance" << "\t" << "threshold" << "\t" << "stddevT" << "\t" << "stdDev" << "\t" << "stddevDEV" << endl;
	for (int j = 0; j < 20; j++) {
		double capacity = 1000.0;
		double energy0 = capacity;
		int life = 1e+5;
		double deltaCharge = 50.0;
		double deltaDischarge = capacity;
		double threshold = 0.0;
		double variance = 5.0*j;
		fileO << variance << "\t";
		std::cout << variance << "\t";
		double mediaT = 0.0;
		double stddevT = 0.0;

		double mediaDEV = 0.0;
		double stddevDEV = 0.0;

		int nRep = 20;
		int Ndays = 100;
		for (int i = 0; i < nRep; i++) {
			vector<Player *> players;
			players.push_back(new NuclearPlant(100.0));
			TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge,1,60,0.0);
			players.push_back(b);
			players.push_back(new Building(24.0, 150.0, variance));
			Simulator s{ players };
			s.Run(24 * Ndays);

			mediaT += b->getThreshold();
			stddevT += pow(b->getThreshold(), 2.0);

			mediaDEV += b->getStdDev();
			stddevDEV += pow(b->getStdDev(), 2.0);

		}
		mediaT = mediaT / (double)nRep;
		stddevT = sqrt(stddevT/ (double)nRep - pow(mediaT, 2.0));

		mediaDEV = mediaDEV / (double)nRep;
		stddevDEV = sqrt(stddevDEV / (double)nRep - pow(mediaDEV, 2.0));

		fileO << mediaT << "\t"<<stddevT<<"\t" << mediaDEV << "\t" << stddevDEV <<endl;
		std::cout << mediaT << "\t" << stddevT << "\t" << mediaDEV << "\t" << stddevDEV << endl;
	}
	fileO.close();



	///*GRAFICO THRESHOLD VS TIME SWITCH*/
	//double capacity = 1000.0;
	//double energy0 = 0.5;
	//int life = 1e+5;
	//double deltaCharge = capacity/5;
	//double deltaDischarge = capacity;
	//double threshold = 0.0;
	//double variance = 0.0;
	//for (int i = 0; i < 1; i++) {
	//	vector<Player *> players;
	//	players.push_back(new NuclearPlant(100.0));
	//	TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge,1,60,0.0);
	//	players.push_back(b);
	//	players.push_back(new Building(24.0, 150.0, variance));
	//	Simulator s{ players };
	//	s.Run(24*100);
	//	LOG<<b->getMinThreshold();
	//	s.dumpResult("C:/Users/edofa/Desktop");
	//	b->DumpDat("C:/Users/edofa/Desktop");
	//}


	///*TEST ADAPTIVEBATTERY*/
	//double capacity = 1000.0;
	//double energy0 = capacity;
	//int life = 1e+5;
	//double deltaCharge = capacity/5;
	//double deltaDischarge = capacity;
	//double threshold = 0.0;
	//double variance = 0.0;
	//for (int i = 0; i < 1; i++) {
	//	vector<Player *> players;
	//	players.push_back(new NuclearPlant(100.0));
	//	TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge,1,60,0.0);
	//	players.push_back(b);
	//	players.push_back(new Building(24.0, 150.0, variance));
	//	Simulator s{ players };
	//	s.Run(24*100);
	//	LOG<<b->getMinThreshold();
	//	s.dumpResult("C:/Users/edofa/Desktop");
	//	//s.dumpResult("C:/Users/Edoardo/Desktop");
	//	//s.dumpResult("C:/Users/utente/Desktop");
	//	b->DumpDat("C:/Users/edofa/Desktop");
	//}

/*----------------------EXP CON RISULTATO CERTO*/
//double capacity = 1000.0;
//double energy0 = 70.0;
//int life = 1e+5;
//double deltaCharge = capacity / 5;
//double deltaDischarge = capacity;
//double threshold = 0.0;
//double variance = 0.0;
//for (int i = 0; i < 100; i++) {
//	vector<Player *> players;
//	players.push_back(new NuclearPlant(100.0));
//	//TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge,50,0);
//	TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 50, 1000);
//	players.push_back(b);
//	players.push_back(new Building(24.0, 150.0, variance));
//	Simulator s{ players };
//	s.Run(51);
//	vector<double> x;
//	vector<double> y;
//	for (int j = 0; j < capacity; j += 10) {
//		x.push_back(j);
//		y.push_back(b->simulate(j));
//		//LOG << b->simulate(j);
//		//LOG << b->simulate(j);
//	}
//	s.dumpResult("C:/Users/Edoardo/Desktop");
//	s.plotXYGraph(x, y, "C:/Users/Edoardo/Desktop", 0);
//
//	LOG << b->getEnergyThreshold();
//	//s.dumpResult("C:/Users/Edoardo/Desktop");
//	//s.dumpResult("C:/Users/utente/Desktop");
//}

/*--------------------------------*/
//double capacity = 1000.0;
//double energy0 = 70.0;
//int life = 1e+5;
//double deltaCharge = capacity / 5;
//double deltaDischarge = capacity;
//double threshold = 0.0;
//double variance = 0.0;
//vector<Player *> players;
//players.push_back(new NuclearPlant(100.0));
////TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge,50,0);
//TvBattery* b = new TvBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 50, 150);
//players.push_back(b);
//players.push_back(new Building(24.0, 150.0, variance));
//Simulator s{ players };
//s.Run(51);
//LOG << b->getEnergyThreshold();

//s.dumpResult("C:/Users/Edoardo/Desktop");
//s.dumpResult("C:/Users/utente/Desktop");


    /*TEST WISEBATTERY*/
	//double capacity = 1000.0;
	//double energy0 = 150.0;
	//int life = 1e+5;
	//double deltaCharge = 10.0;
	//double deltaDischarge = capacity;
	//double threshold = 1000.0;
	//vector<Player *> players;
	//players.push_back(new NuclearPlant(100.0));
	//EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, -1.0, threshold);
	//players.push_back(b);
	//WiseBattery* wb = new WiseBattery(capacity, energy0, life, deltaCharge, deltaDischarge, 10,-1);
	//players.push_back(wb);
	//double variance = 10.0;
	//players.push_back(new Building(24.0, 110.0, variance));
	//Simulator s{ players };
	////s.Run(1);
	//wb->Brain(false);

	/*TEST STABILITA'*/
	//for (int count = 0; count < 5; count++){
	//	vector<double> x;
	//	vector<double> y;
	//	for (int i = 0; i < 10; i++){
	//		double capacity = 1000.0;
	//		double energy0 = 150.0;
	//		int life = 1e+5;
	//		double deltaCharge = 10.0;
	//		double deltaDischarge = capacity;
	//		double threshold = 0.0;
	//		vector<Player *> players;
	//		players.push_back(new NuclearPlant(100.0));
	//		EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, threshold);
	//		players.push_back(b);
	//		players.push_back(new Building(24.0, 110.0, 40.0));
	//		Simulator s{ players};
	//		x.push_back(100.0*i);
	//		b->changeThresholdTo(100.0*i);
	//		for (int j = 0; j < 1000; j++){
	//			s.Run(1000); // vado avanti per 1000 turni
	//		}
	//		b->printStatistics();
	//		y.push_back(b->getMeanPayOff());
	//	}
	//	//(new Simulator())->plotXYGraph(x, y, "C:/Users/edofa/Documents/BitBucket/electricgames/outputHtml",0 );
	//	(new Simulator())->plotXYGraph(x, y, "C:/Users/Edoardo/Desktop", count);
	//}


	/*TEST AGGIORNAMENTO*/ 
	//double capacity = 1000.0;
	//double energy0 = 150.0;
	//int life = 1e+5;
	//double deltaCharge = capacity;
	//double deltaDischarge = capacity;
	//double variancechangeThreshold = -1;
	//double threshold = 0.0;
	//vector<Player*> players;
	//players.push_back(new NuclearPlant(100.0));
	//EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, variancechangeThreshold, threshold);
	//players.push_back(b);
	//players.push_back(new Building(24.0, 110.0, 60.0));
	//Simulator s{ players };
	//for (int i = 0; i < 50; i++) {
	//	for (int j = 0; j < 100; j++) {
	//		s.Run(1000); // vado avanti per 1000 turni	
	//	}
	//	s.UpdateBatteriesThreshold(DETEVO, true);
	//}
	//b->printStatistics();
	
	
	/* GRAFICO VARIANZA THRESHOLD */
	//ofstream fileO("C:/Users/edofa/Desktop/data.txt");
	//fileO << "data = ["<<endl;
	//for (int ii = 1; ii < 10; ii++){
	//	double varianceBuilding = 10.0*ii;
	//	fileO << varianceBuilding << ";";
	//	double capacity = 1000.0;
	//	double energy0 = 150.0;
	//	int life = 1e+5;
	//	double deltaCharge = capacity;
	//	double deltaDischarge = capacity;
	//	double variancechangeThreshold = -1;
	//	double threshold = 0.0;
	//	double somma = 0.0;
	//	for (int count = 0; count < 20; count++) {
	//		vector<Player*> players;
	//		players.push_back(new NuclearPlant(100.0));
	//		EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, variancechangeThreshold, threshold);
	//		players.push_back(b);
	//		players.push_back(new Building(24.0, 110.0, varianceBuilding)); 
	//		Simulator s{ players };
	//		for (int i = 0; i < 20; i++) {
	//			for (int j = 0; j < 100; j++) {
	//				s.Run(1000); // vado avanti per 1000 turni	
	//			}
	//			s.UpdateBatteriesThreshold(DETEVO, false);
	//		}
	//		fileO << b->getEnergyThreshold() << ";";
	//		LOG << "count: " << count << " ii: " << ii;
	//	}
	//	fileO << endl;
	//}
	//fileO.close();
	
	
	/* DUE BATTERIE */
	//double capacity = 1000.0;
	//double energy0 = 150.0;
	//int life = 1e+5;
	//double deltaCharge = capacity;
	//double deltaDischarge = capacity;
	//double variancechangeThreshold = -1;
	//double threshold = 0.0;
	//vector<Player*> players;
	//players.push_back(new NuclearPlant(100.0));
	//EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, capacity/10.0, threshold);
	//players.push_back(b);
	//EnergyBattery* b1 = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, capacity / 100.0, threshold);
	//players.push_back(b1);
	//players.push_back(new Building(24.0, 110.0, 60.0));
	//Simulator s{ players };
	//int nVite = 20;
	//for (int i = 0; i < nVite; i++) {
	//	for (int j = 0; j < 100; j++) {
	//		s.Run(1000); // vado avanti per 1000 turni	
	//	}
	//	s.UpdateBatteriesThreshold(DETEVO, true, nVite);
	//}
	

	//vector<double> x;
	//vector<double> y;
	//for (int i = 0; i < 10; i++) {
	//	double varianceBuilding = 10.0*i;
	//	x.push_back(varianceBuilding);
	//	double somma = 0.0;
	//	for (int count = 0; count < 20; count++) {
	//		double capacity = 1000.0;
	//		double energy0 = 150.0;
	//		int life = 1e+5;
	//		double deltaCharge = capacity;
	//		double deltaDischarge = capacity;
	//		double variancechangeThreshold = -1;
	//		double threshold = 0.0;
	//		vector<Player*> players;
	//		players.push_back(new NuclearPlant(100.0));
	//		EnergyBattery* b = new EnergyBattery(capacity, energy0, life, deltaCharge, deltaDischarge, variancechangeThreshold, threshold);
	//		players.push_back(b);
	//		players.push_back(new Building(24.0, 110.0, varianceBuilding)); // 60 è un buon risultato 
	//		Simulator s{ players };
	//		for (int i = 0; i < 20; i++) {
	//			for (int j = 0; j < 100; j++) {
	//				s.Run(1000); // vado avanti per 1000 turni	
	//			}
	//			s.UpdateBatteriesThreshold(DETEVO, false);
	//		}
	//		somma += b->getEnergyThreshold();
	//		LOG << "count: " << count << " i: " << i;
	//		LOG << " ";
	//		LOG << " ";
	//	}
	//	y.push_back(somma/20.0);
	//}

	////(new Simulator())->plotXYGraphTXT(x, y, "C:/Users/edofa/Desktop"); // portatile
	//(new Simulator())->plotXYGraphTXT(x, y, "C:/Users/Edoardo/Desktop"); // fisso

	




	std::cin.get();
    return 0;
}
