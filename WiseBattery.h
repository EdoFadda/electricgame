// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_WISEBATTERY_H
#define EG_WISEBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include "string"
#include "Player.h"
#include "Battery.h"
#include "EnergyBattery.h"



struct WiseBatteryState {
	int ResidualLife;
	double Energy;
	double Transfert;
	double Ask;
	double Bid;
	double AskReal;
	double BidReal;
	double PayOff;
};

class WiseBattery : public Battery {
	/*
	la wise battery prende in input lo stato attuale delle altre batterie (energia attuale e energia threshold voluto) e una previsione dei consumi. 
	In base a questo decide cosa fare. 
	*/
	
public:

	WiseBattery(double cap, double energy0, int life, double DeltaCharge, double DeltaDischarge,int timeHorizonForecast, double varianceChangeThreshold) : Battery(cap, energy0, life) {
		this->DeltaCharge = DeltaCharge;
		this->DeltaDischarge = DeltaDischarge;
		this->Energy = energy0;
		this->Capacity = cap;
		this->ResidualLife = life;
		this->timeHorizonForecast = timeHorizonForecast;
		this->presentStat = { 0,0.0,0.0 };
		this->bestStat = { -1, 0.0, 0.0 };
		BatteryHistory.push_back({ ResidualLife, Energy, 0.0,0.0,0.0,0.0,Payoff });
		if (varianceChangeThreshold <= 0.0) {
			this->varianceChangeThreshold = cap / 10.0;
		}
		else {
			this->varianceChangeThreshold = varianceChangeThreshold;
		}
		this->presentLife = 0;
	}


	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);
	void Transfer(systemState s, double amount);
	
	void setPlayers(vector<Player *>&  p) {
		this->neighbors = p;
	}
	
	void newThreshold(int version, bool verbose = false, int nVite = 0); // comando da usare quando finisco un set di vite
	void setVarianceChangeThreshold(double value);

	void setHorizonForecast(int t); // imposta il numero di istanti futuri che può considerare la batteria

	void startSimulation();
	void endSimulation();

	void DumpHtml(string path);
	void printStatistics();
	
	double getEnergy();
	double getMeanPayOff();
	double Brain(bool forced);
	
	
private:
	// technical data:
	int ResidualLife;
	int presentLife;
	double Energy;
	double Load;
	double Capacity;
	double DeltaDischarge;
	double DeltaCharge;

	vector<Player*> neighbors;

	// decisional data LONG

	double Threshold;
	double BestEnergyThreshold;
	int timeHorizonForecast;
	double varianceChangeThreshold;
	//double QuantileThreshold = 1.281552; // 0.9 quantile
	//double QuantileThreshold = 0.8416212; // 0.8 quantile
	double QuantileThreshold = 0.5244005; // 0.7 quantile

	// decision data SHORT
	double ask;
	double bid;
	vector<double> forecast;

	// statistics of past lives(with different coefficients):
	statistics bestStat;

	// statistics of lives:
	statistics presentStat;

	// SystemData
	std::vector<Battery*> SystemBatteries; // qua memorizzo le altre batterie

	// HistoricalData:
	std::vector<WiseBatteryState> BatteryHistory; // qua memorizzo la storia della Batteria nella sua vita corrente
	std::vector<systemState>  SystemStates;


	double sign(double x) {
		if (x > 0) 
			return 1;
		if (x < 0)
			return -1;
		else
			return 0;
	}

	double computeBlackOutProbability(double** scenariosMatrix, int nScenarios, int timeHorizonForecast) {
		int nBlackOut = 0;
		for (int i = 0; i < nScenarios; i++) {
			for (int t = 0; t < timeHorizonForecast; t++) {
				if (scenariosMatrix[i][t] < 0) {
					nBlackOut++;
					break;
				}
			}
		}
		return (nBlackOut+0.0)/ (nScenarios+0.0);
	}

	double computeNotFullChargeProbability(double** scenariosMatrix, int nScenarios, int timeHorizonForecast) {
		int nNotFullCharge = 0;
		int nChargeToFull = ceil((Capacity-Energy)/DeltaCharge);
		//nChargeToFull = 5;
		//cout << nChargeToFull << endl;
		for (int i = 0; i < nScenarios; i++) {
			int nCharge = 0;
			for (int t = 0; t < timeHorizonForecast; t++) {
				if (scenariosMatrix[i][t] > 0)
					nCharge++;
				else
					break;
			}
			if (nCharge < nChargeToFull)
				nNotFullCharge++;
		}

		return min((nNotFullCharge+0.0) / (nScenarios+0.0)+0.01,1.0);	// aggiungo una piccola probabilità per prevenire alcuni casi come 
																		// se nChargeToFull>timeHorizonForecast
																		// se scenarioMatrix[i][t]==0 c'è anche lo stato NORMAL
	}

	void simulateBatteries(double* scenario, int timeHorizonForecast, vector<double>& thresholds, vector<double>& energies, vector<double>& ask, bool wiseCharge) {
		vector<bool> caricaScarica;
		double absorbed = 0.0;
		for (int i = 0; i < energies.size(); i++)
			caricaScarica.push_back(0);

		for (int t = 0; t < timeHorizonForecast; t++) {
			//LOG << "INIZIO TEMPO " << t;
			//LOG << "energyProfile e' di " << scenario[t] << " mentre le energie nelle batterie sono:";
			if (scenario[t] > 0) { // se c'è abbondanza di energia [UTOPY-NORMAL]
				double reqEnergy = 0.0;
				// suppongo che mi chiedano thresholds[i] - energies[i] tutti
				for (int i = 0; i < energies.size(); i++) {
						//cout << energies[i] << " (T:" << thresholds[i] << ")" << "\t";
					if (energies[i] < thresholds[i] || caricaScarica[i]==1) {
						reqEnergy += ask[i];
						caricaScarica[i] = 1;
					}
					if (wiseCharge) reqEnergy += this->DeltaCharge;
				}
					//cout << endl;
					//LOG << "EnergiaRichista dalle batterie: " << reqEnergy;
				if (reqEnergy < scenario[t]) {
					//LOG << "UTOPY ";
					for (int i = 0; i < energies.size(); i++)
						if(caricaScarica[i]) energies[i] += ask[i];

					if (wiseCharge) absorbed += this->DeltaCharge;
					scenario[t] = scenario[t] - reqEnergy;
					//LOG << "energyProfile in "<<t<<" diventa:"<<scenario[t];
				}else {
					//LOG << "NORMAL ";
					for (int i = 0; i < energies.size(); i++)
						energies[i] += ask[i]*(scenario[t]/reqEnergy);
					if (wiseCharge) absorbed += this->DeltaCharge*(scenario[t] / reqEnergy);
					scenario[t] = 0;
				}
			}else{ // se c'è carenza [DIFFICULT-BLACKOUT]
				wiseCharge = false;
				double sumEnergy = 0.0;
				for (int i = 0; i < energies.size(); i++) {
					//cout << energies[i] << " (T:" << thresholds[i] << ")" << "\t";
					sumEnergy += energies[i];
				}
				sumEnergy += this->Energy;
				//cout << endl;
				// se non riesco a soddisfare ho un black out oppure difficult
				double wiseDelta = this->Energy + absorbed;
				if (sumEnergy + scenario[t] + wiseDelta < 0) {
					//LOG << "BLACK OUT ";
					for (int i = 0; i < energies.size(); i++) {
						energies[i] = 0.0;
						scenario[t] = sumEnergy + scenario[t];
						caricaScarica[i] = 0;
					}
				}else {
					for (int i = 0; i < energies.size(); i++) {
						//LOG << "DIFFICULT ";
						energies[i] = energies[i] * (1 + scenario[t] / sumEnergy); // scenario[t] è negativo perché è una richiesta
						absorbed -= (scenario[t] / sumEnergy);
						scenario[t] = 0;
						caricaScarica[i] = 0;
					}
				}
			}
			//LOG << "FINE TEMPO "<<t;
			//LOG << "---------------------";

		}
	}

};

#endif //EG_BATTERY_H
