// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_NEURALBATTERY_H
#define EG_NEURALBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <random>
#include "string"




struct NeuralBatteryState {
	double ResidualLife;
	double Energy;
	double Transfert;
	double Ask;
	double Bid;
	double AskReal;
	double BidReal;
	double PayOff;
};

class NeuralBattery : public Battery {
public:

	NeuralBattery(double cap, double energy0, int life, double DeltaCharge,double DeltaDischarge, int memory) : Battery(cap, energy0, life) {
		this->DeltaCharge = DeltaCharge;
		this->DeltaDischarge = DeltaDischarge;
		this->Energy = energy0;
		if (energy0 == 0)
			LOG << "Please set energy0 to be higher, otherwise the deterministic evolution of the threshold will suffer.";
		this->Capacity = cap;
		this->ResidualLife = life;
		this->Payoff = 0.0;
		this->presentStat = { 0,0.0,0.0 };
		this->bestStat = { -1, 0.0, 0.0};
		BatteryHistory.push_back({ResidualLife, Energy, 0.0,0.0,0.0,0.0,Payoff});
		this->presentLife = 0;
		this->weight = new double[memory - 1 + 2];
	}

	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);
	void Transfer(systemState s, double amount);


	void computeWeights();
	
	void setPlayers(vector<Player *>&  p) {
		this->neighbors = p;
	}


	void DumpHtml(string path);
	
	void generaScenari(int nScenarios, double** scenariosMatrix);

	double getEnergy();

	double getDeltaCharge();

	double getMeanPayOff();
	double Brain(systemState s, bool forced);
	
private:
	int memory;
	vector<Player*> neighbors;
	
	// technical data:
	double ResidualLife;
	double Energy;
	double Load;
	double Capacity;
	double DeltaDischarge;
	double DeltaCharge;

	// decisional data LONG
	int presentLife;
	int timeHorizonForecast;
	double* weight;


	// decision data short SHORT
	double ask;
	double bid;

	// statistics of past lives(with different coefficients):
	statistics bestStat;

	// statistics of lives:
	statistics presentStat;


	// HistoricalData:
	std::vector<NeuralBatteryState> BatteryHistory; // qua merizzo la storia della Batteria nella sua vita corrente
	std::vector<systemState>  SystemStates;


	double sign(double x) {
		if (x > 0) 
			return 1;
		if (x < 0)
			return -1;
		else
			return 0;
	}

	void simulateBatteries(double* scenario, int timeHorizonForecast, vector<double>& thresholds, vector<double>& energies, vector<double>& ask, bool wiseCharge) {
		vector<bool> caricaScarica;
		double absorbed = 0.0;
		for (int i = 0; i < energies.size(); i++)
			caricaScarica.push_back(0);

		for (int t = 0; t < timeHorizonForecast; t++) {
			//LOG << "INIZIO TEMPO " << t;
			//LOG << "energyProfile e' di " << scenario[t] << " mentre le energie nelle batterie sono:";
			if (scenario[t] > 0) { // se c'è abbondanza di energia [UTOPY-NORMAL]
				double reqEnergy = 0.0;
				// suppongo che mi chiedano thresholds[i] - energies[i] tutti
				for (int i = 0; i < energies.size(); i++) {
					//cout << energies[i] << " (T:" << thresholds[i] << ")" << "\t";
					if (energies[i] < thresholds[i] || caricaScarica[i] == 1) {
						reqEnergy += ask[i];
						caricaScarica[i] = 1;
					}
					if (wiseCharge) reqEnergy += this->DeltaCharge;
				}
				//cout << endl;
				//LOG << "EnergiaRichista dalle batterie: " << reqEnergy;
				if (reqEnergy < scenario[t]) {
					//LOG << "UTOPY ";
					for (int i = 0; i < energies.size(); i++)
						if (caricaScarica[i]) energies[i] += ask[i];

					if (wiseCharge) absorbed += this->DeltaCharge;
					scenario[t] = scenario[t] - reqEnergy;
					//LOG << "energyProfile in "<<t<<" diventa:"<<scenario[t];
				}
				else {
					//LOG << "NORMAL ";
					for (int i = 0; i < energies.size(); i++)
						energies[i] += ask[i] * (scenario[t] / reqEnergy);
					if (wiseCharge) absorbed += this->DeltaCharge*(scenario[t] / reqEnergy);
					scenario[t] = 0;
				}
			}
			else { // se c'è carenza [DIFFICULT-BLACKOUT]
				wiseCharge = false;
				double sumEnergy = 0.0;
				for (int i = 0; i < energies.size(); i++) {
					//cout << energies[i] << " (T:" << thresholds[i] << ")" << "\t";
					sumEnergy += energies[i];
				}
				sumEnergy += this->Energy;
				//cout << endl;
				// se non riesco a soddisfare ho un black out oppure difficult
				double wiseDelta = this->Energy + absorbed;
				if (sumEnergy + scenario[t] + wiseDelta < 0) {
					//LOG << "BLACK OUT ";
					for (int i = 0; i < energies.size(); i++) {
						energies[i] = 0.0;
						scenario[t] = sumEnergy + scenario[t];
						caricaScarica[i] = 0;
					}
				}
				else {
					for (int i = 0; i < energies.size(); i++) {
						//LOG << "DIFFICULT ";
						energies[i] = energies[i] * (1 + scenario[t] / sumEnergy); // scenario[t] è negativo perché è una richiesta
						absorbed -= (scenario[t] / sumEnergy);
						scenario[t] = 0;
						caricaScarica[i] = 0;
					}
				}
			}
			//LOG << "FINE TEMPO "<<t;
			//LOG << "---------------------";

		}
	}
};

#endif //EG_BATTERY_H
