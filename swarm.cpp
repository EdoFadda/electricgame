
#include <sstream>
#include <string>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <deque>
#include <iomanip>
#include <vector>
#include "swarm.h"
#include "particle.h"
#include "objectiveFunction.h"


swarm::swarm(int inputDim, int nParticles,  int nEpochs,float V_MAX,double* RANGE_MIN, double* RANGE_MAX, objectiveFunction* ObjFunc, double omega, double phi_ind, double phi_glb){
	// omega: how much the pat speed affect the present speed (usually 1.0)
	// phi_ind: how much the individual best solution found affect the  present speed (usually 2.0)
	// phi_glb: how much the best solution found by the swarm affect the present speed (usually 2.0)


	this->inputDim = inputDim;
	this->nParticles = nParticles;
	this->V_MAX = V_MAX;
	this->START_RANGE_MIN = START_RANGE_MIN;
	this->START_RANGE_MAX = START_RANGE_MAX;
	this->nEpochs = nEpochs;
	particles = new cParticle[nParticles];

	double* minValues = new double[inputDim];
	double* maxValues = new double[inputDim];
	for (int i = 0; i < inputDim; i++) {
		minValues[i] = RANGE_MIN[i];
		maxValues[i] = RANGE_MAX[i];
	}

	
	for (int i = 0; i < nParticles; i++) {
		particles[i] = *(new cParticle(inputDim,minValues,maxValues, nEpochs,ObjFunc,omega,phi_ind,phi_glb));
	}
	

};


void swarm::psoAlgorithm(bool verbose){
    int best = 0;
    int epoch = 0;
    bool done = false;

	for (epoch = 0; epoch < nEpochs; epoch++) {
		if (verbose) {
			cout << "INIZIO EPOCA" << endl;
			for (int i = 0; i < nParticles; i++) {
				particles[i].print();
			}
		}
        best = minimum();

        //If any particle's pBest value is better than the gBest (the historical best) value,
        //make it the new gBest Value.
        if(particles[best].getOF() < globalBest){
			globalBest = particles[best].getOF();
        }

        updateParticles(best);
		if (verbose)cout << "FINE EPOCA" << endl;
     }


	bestParticle = particles + best;

	if (verbose) cout << nEpochs << " epochs completed." << endl;
}



void swarm::updateParticles(int bestIndex){

    for(int i = 0; i < nParticles; i++){
		particles[i].copyGlobalOptimalPositionFrom((particles+bestIndex));

		particles[i].updatePosition();
    }

}



int swarm::minimum(){
	int best = 0;
	double bestValue = particles[0].getOF();

	for (int i = 1; i < nParticles; i++) {
		if (particles[i].getOF() < bestValue) {
			best = i;
			bestValue = particles[0].getOF();
		}
	}
	return best;

}

void swarm::printBest(){
	double* pos = new double[inputDim];
	bestParticle->getBestPosition(pos); // dalla particella che ha raggiunto il miglior valore dall'inizio mi faccio ritornare il suo best
	cout<<"BEST value found: "<<bestParticle->getBestOF()<<endl;
	cout << "BEST solution found: ";
	for (int i = 0; i < inputDim; i++) {
		cout << pos[i] << "\t";
	}
	cout << endl;
}

void swarm::returnBestPosition(double* ris) {
	bestParticle->getBestPosition(ris);
}