// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "TvBattery.h"
#include <limits.h>

using namespace std;


# define CHARGE_CODE  1
# define DISCHARGE_CODE  -1



double TvBattery::Ask(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf > 0) {
		return bf;
	}
	else {
		return 0.0;
	}

}

double TvBattery::Bid(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf < 0) {
		return -bf;
	}
	else {
		return 0.0;
	}
}

void TvBattery::Transfer(systemState s, double amount) {
	// amount negativo -> ceduto energia
	// amount positivo -> acqistato energia

	// Memorizzo lo stato del sistema
	SystemStates.push_back(s);

	// Arrange life
	if (BatteryHistory.size() == 0 ){
		ResidualLife--;
	}else{
		if (amount > 0 && sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert)) {
			//ResidualLife -=  tanh(Energy) + 1.0;
			ResidualLife--;
		}
	}
	
	
	// update Energy 
	Energy = min(Energy + amount, Capacity);
	
	

	double Ask = 0.0;
	if (!s.blackOut && !s.danger) {
		if (Energy < Threshold) { // se ho meno energia del threshold allora continuo a caricare
			Ask = min(DeltaCharge, Capacity - Energy);
		}
		else {
			// se ho più energia del threshold allora 
			// al primo istante cerco subito di caricarmi
			if (BatteryHistory.size() == 0) {
				if (STARTEnergy < Threshold)
					Ask = min(DeltaCharge, Capacity - Energy);
				else
					Ask = 0.0;
			}
			else {
				// se l'istante prima mi stavo caricando continuo
				if (sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) == CHARGE_CODE) {
					Ask = min(DeltaCharge, Capacity - Energy);
				}
			}
		}
	}

	// update BatteryHistory
	TvBatteryState bS = { ResidualLife, Energy, amount, FromNonStorage, Ask};
	BatteryHistory.push_back(bS);


	if (presentLife % updateInterval == 0 && presentLife > 0) {
		//LOG << "UPDATE:  " << presentLife;
		this->update();
	}
	// update presentife
	presentLife++;
}

void TvBattery::addFromNonStorage(double fromNonStorage) {
	this->FromNonStorage = fromNonStorage;
}


// output methods
double TvBattery::getEnergy() {
	return Energy;
}

double TvBattery::getThreshold(){
	return Threshold;
}

double TvBattery::getMinThreshold(){
	return minThreshold;
}

double TvBattery::getStdDev() {
	return StdDev;
}



double TvBattery::getDeltaCharge(){
	return this->DeltaCharge;
}

double TvBattery::getResidualLife(){
	return this->ResidualLife;
}


double TvBattery::Brain(bool forced) {
	
	if (forced) {
		return -min(DeltaDischarge, Energy);
	}
	else {
		if (Energy < Threshold) { // se ho meno energia del threshold allora continuo a caricare
			return min(DeltaCharge, Capacity - Energy);
		}
		else {
			// se ho più energia del threshold allora 
			// al primo istante cerco subito di caricarmi
			if (BatteryHistory.size() == 0) {
				if (STARTEnergy < Threshold)
					return min(DeltaCharge, Capacity - Energy);
				else
					return 0.0;
			}else {
				// se l'istante prima mi stavo caricando continuo
				if (sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) == CHARGE_CODE) {
					return min(DeltaCharge, Capacity - Energy);
				}
			}
		}
	}
}


double TvBattery::simulate(double threshold, double safety, bool verbose){
	// return true if with threshold there would be a black out, false otherwise
	double obj = ((double)STARTResidualLife);
	//ofstream fileO("C:/Users/Edoardo/Desktop/log.txt");

	if (verbose) {
		LOG << "Try with threshold " << threshold;
		//fileO << "Try with threshold " << threshold << endl;
	}
	

	vector<double> pastEnergy;
	bool chargeBefore = false;
	//pastEnergy.push_back(STARTEnergy);
	//pastEnergy.push_back(STARTEnergy);
	pastEnergy.push_back(0.0);
	pastEnergy.push_back(0.0);
	int esponente = 0;
	double variation = 0.0;

	for (int t = 1; t < SystemStates.size(); t++) {
		 esponente = t - SystemStates.size()+1;
		 variation = 0.0;
		//cout << exp(forgetRate*((double)esponente)) << "\t ";
		//if (t%24 == 0 || t % 23 == 0) {
		//	verbose = true;
		//}else {
		//	verbose = false;
		//}

		double Ask = 0.0;
		double ForcedBid = pastEnergy[t];
		double correzioneBid = -BatteryHistory[t-1].Energy + pastEnergy[t];
		if (verbose){
			LOG << "Time: " << t;
			LOG << "AskForced(Bid): " << SystemStates[t].totAskForced<<" ("<< SystemStates[t].totBid<<")";
			LOG << "BidForced: " << SystemStates[t].totBidForced + correzioneBid; //<<"( "<< SystemStates[t].totBidForced<<" - "<< BatteryHistory[t].Energy <<"+"<< pastEnergy[t] <<" )";
			//fileO << "Time: " << t<<endl;
			//fileO << "AskForced(Bid): " << SystemStates[t].totAskForced << " (" << SystemStates[t].totBid << ")" << endl;
			//fileO << "BidForced: " << SystemStates[t].totBidForced - BatteryHistory[t].Energy + pastEnergy[t] << endl;
		}
		

		// CLASSIFICO GLI STATI:
		double totAskForced = SystemStates[t].totAskForced;
		if (totAskForced > SystemStates[t].totBid) { // se c'è più domanda
			totAskForced = totAskForced*(1.0 + safety);
			if (SystemStates[t].totBidForced + correzioneBid < totAskForced) {
					if(verbose) LOG << "BLACK OUT ";
					//if (verbose) fileO << "BLACK OUT ";
					pastEnergy.push_back(0.0);
					chargeBefore = false;
					variation = -100000*Capacity; /**********************/
					if (verbose) LOG << "-- "<< variation;
					if (t > SystemStates.size()*0.2) return FLAG_BLACKOUT;
			}else{
				
				double perc = (totAskForced - BatteryHistory[t].FromNonStorage) / (SystemStates[t].totBidForced+correzioneBid - BatteryHistory[t].FromNonStorage);
				//LOG <<"--> " <<(SystemStates[t].totAskForced - BatteryHistory[t].FromNonStorage) << " " << (SystemStates[t].totBidForced + correzioneBid - BatteryHistory[t].FromNonStorage);
				
				if (verbose) {
					LOG << "DANGER";
					LOG << "Initial Energy: " << pastEnergy[t];
					LOG << "------> " << totAskForced;
					LOG << "------> " << BatteryHistory[t].FromNonStorage;
					LOG << "------> " << SystemStates[t].totBidForced + correzioneBid;
					LOG << "Perc: " << perc;
					LOG << "Transfer: " << pastEnergy[t]*perc;
					LOG << "Final Energy: " << pastEnergy[t]*(1.0 - perc);
					//fileO << "DANGER" << endl;
					//fileO << "Initial Energy: " << pastEnergy[t] << endl;
					//fileO << "Perc: " << perc << endl;
					//fileO << "Transfer: " << pastEnergy[t] * perc << endl;
					//fileO << "Final Energy: " << pastEnergy[t] * (1.0 - perc) << endl;
				}
				pastEnergy.push_back(pastEnergy[t] * (1.0 - perc));
				chargeBefore = false;
			}
		}
		else { 
			// se c'è più offerta
			if (pastEnergy[t] < threshold || chargeBefore) {
				Ask = min(DeltaCharge, Capacity - pastEnergy[t]);
			}

			if (SystemStates[t].totAsk - BatteryHistory[t].Transfert + Ask <= SystemStates[t].totBid) {
				
				if (verbose) {
					LOG << "UTOPY" ;
					LOG << "Initial Energy: " << pastEnergy[t]<<" ("<< threshold <<")";
					LOG << "Ask: " << Ask;
					LOG << "Final Energy: " << pastEnergy[t]+Ask << " (" << threshold << ")";
					//fileO << "UTOPY" << endl;
					//fileO << "Initial Energy: " << pastEnergy[t] << " (" << threshold << ")" << endl;
					//fileO << "Ask: " << Ask << endl;
					//fileO << "Final Energy: " << pastEnergy[t] + Ask << " (" << threshold << ")" << endl;
				}
				//pastEnergy.push_back(pastEnergy[t] + Ask);
				pastEnergy.push_back(pastEnergy[t] + Ask);
				if (!chargeBefore && Ask > 0) {
					variation = - pastEnergy[t]-1; /**********************/
					if (verbose) LOG << "-- "<<variation;

				} 
				if(Ask > 0)chargeBefore = true; 
				else chargeBefore = false;

			}else {
				
				double perc = 0.0;
				double DeltaAsk = 0.0;
				if (Ask > 0) {
					DeltaAsk = Ask - BatteryHistory[t].Ask;
					// dentro perc c'è già contata la richiesta che ho fatto nel mondo realmente esistente
					perc = (SystemStates[t].totBid - SystemStates[t].totAskForced) / (DeltaAsk + SystemStates[t].totAsk - SystemStates[t].totAskForced);
				}
				if (verbose) {
					LOG <<  "NORMAL";
					LOG << "Initial Energy: " << pastEnergy[t];
					LOG << "Percentage Satisfy (Ask): " << perc<<" ( of "<<Ask<<" )";
					LOG << "New Energy: " << pastEnergy[t] + perc*Ask;
					LOG << "------> " << (SystemStates[t].totBid - SystemStates[t].totAskForced);
					LOG << "------> " << (Ask + SystemStates[t].totAsk - SystemStates[t].totAskForced);
					LOG << "------> " << DeltaAsk;
				}
				pastEnergy.push_back(pastEnergy[t] + perc*Ask);
				if (!chargeBefore && Ask > 0) {
					variation = - pastEnergy[t-1]-1;/**********************/
					if (verbose) LOG << "-- "<<variation;
				} 
				if (Ask > 0) chargeBefore = true;
				else chargeBefore = false;
			}
		}

		obj += exp(forgetRate*((double)esponente))*variation;

		//cout << exp(forgetRate*((double)esponente)) << " ";

		if (verbose) {
			cout << endl;
			//fileO << endl;
		}
	}
	//cout << endl;
	
	//fileO.close();
	//double minEnergy = pastEnergy[0];
	//for (int i = 0; i < pastEnergy.size(); i++) {
	//	if (pastEnergy[i] < minEnergy)
	//		minEnergy = pastEnergy[i];
	//}
	//obj += minEnergy;

	return obj;
}

void TvBattery::update_1fifth(bool verbose){
	verbose = false;
	
	double ottimoAttuale = simulate(Threshold,verbose);
	if (verbose) LOG << "PARTO CON THRESHOLD: " << Threshold<<" punteggio: "<<ottimoAttuale;
	int improvement = 0;
	int nTrials = 10;
	// genero 10 figli da una normale	
	for (int i = 0; i < nTrials; i++) {
		static random_device r;
		static mt19937 gen{ r() };
		//gen.seed(time(NULL) + this->sequence);
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 
		double ThresholdTemp = abs(this->Threshold + this->StdDev*d(gen));
		if(ThresholdTemp>Capacity) ThresholdTemp = ThresholdTemp - floor(ThresholdTemp/Capacity)*Capacity;

		double ris = simulate(ThresholdTemp,verbose);
		if (verbose) LOG << "NuovoThreshold: " << ThresholdTemp <<" risultato:"<< ris<<" ("<<ottimoAttuale<<")";
		if (ris > ottimoAttuale) {
			if (verbose) {
				LOG << ris << " > " << ottimoAttuale << " (quindi aggiorno)" << ris;
				LOG << "Nuovo Threshold: " << ThresholdTemp;
				cout << endl;
			} 
			Threshold = ThresholdTemp;
			ottimoAttuale = ris;
			improvement++;
		}
	}

	verbose = false;
	double ratio = ((double)improvement) / ((double)nTrials);
	// allargo la varianza se ratio > 0.2
	if (verbose && ratio > 0.2&&StdDev>0.5) {
		LOG << "Aumento la varianza " << this->StdDev;
	}
	if (verbose && ratio < 0.2&&StdDev>0.5) {
		LOG << "Diminuisco la varianza " << this->StdDev;
	}

	this->StdDev = max(this->StdDev*exp(ratio - 0.2),1);
	

}


void TvBattery::update(bool verbose) {

	//simulate(0.0, true);

	int nTrials = 10;
	static random_device r;
	static mt19937 gen{ r() };
	gen.seed(time(NULL));

	// 1_ Genero le varianze
	double* stdDevs = new double[nTrials];
	for (int i = 0; i < nTrials; i++) {
		double err = 0.1*(2.0*(double)rand() / (RAND_MAX)-1);
		stdDevs[i] = StdDevES*(1.0 + err);
		if (stdDevs[i] > Capacity) stdDevs[i] = stdDevs[i] - floor(stdDevs[i] / Capacity)*Capacity;
		
	}
	stdDevs[nTrials - 1] = 50; // Non male per lo switch

	// 2_ Campiono 10 threshold con le varianze calcolate e calcolo il migliore
	double obj;// = simulate(ThresholdES);
	double ThresholdOBJ = simulate(Threshold);

	for (int ii = 0; ii < nGenerations; ii++) {
		obj = -DBL_MAX;
		for (int i = 0; i < nTrials; i++) {
			normal_distribution<> d(ThresholdES, stdDevs[i]); // normal distribution with mean 0 and standard deviation 
			double ThresholdTemp = d(gen);
			//double ThresholdTemp = ThresholdES*(1 + stdDevs[i] * ((double)rand() / (RAND_MAX)));
			if (ThresholdTemp > Capacity) ThresholdTemp = ThresholdTemp - floor(ThresholdTemp / Capacity)*Capacity;
			if (ThresholdTemp < minThreshold) ThresholdTemp = min(minThreshold*(1 + 0.1*(double)rand() / (RAND_MAX)),Capacity);
			double comp = 0.0;
			if (ThresholdTemp < minThreshold*(1.1)) {
				comp = simulate(ThresholdTemp,0.2);
			}
			else {
				comp = simulate(ThresholdTemp);
			}
			if (comp == FLAG_BLACKOUT && avoidBO) {
				//cout << minThreshold << "  sostituito con  " << ThresholdTemp << endl;
				minThreshold = ThresholdTemp*(1.1);
			}else if(comp != FLAG_BLACKOUT){
				if (comp > obj) {
					ThresholdES = ThresholdTemp;
					StdDevES = stdDevs[i];
					obj = comp;
				}
				if (obj > ThresholdOBJ) {
					Threshold = ThresholdES;
					StdDev = StdDevES;
					ThresholdOBJ = obj;
				}
			}
		}
	}

	ThresholdESEvolution.push_back(ThresholdES);

	
	StdDevESEvolution.push_back(StdDevES);
	


	ThresholdEvolution.push_back(Threshold);
	StdDevEvolution.push_back(StdDev);
	
}


void TvBattery::printThreshold() {
	LOG << this->Threshold;
}

void TvBattery::printHistory() {
	
	LOG << "Initial Condition";
	LOG << "Energy: " << STARTEnergy;
	cout << endl;

	for (int i = 0; i < BatteryHistory.size()-1; i++) {
		LOG << "Time " << i;
		LOG << "Energy(at the end of the time step): " << BatteryHistory[i].Energy;
		LOG << "Trans(during the time step): " << BatteryHistory[i].Transfert;
		LOG << "FNS: " << BatteryHistory[i].FromNonStorage;
		
		
		if (SystemStates[i].blackOut) LOG << "BLACK OUT ";
		if (SystemStates[i].danger) LOG << "DANGER ";
		if (SystemStates[i].normal) LOG << "NORMAL ";
		if(SystemStates[i].utopy) LOG << "UTOPY ";

		LOG << "Ask: " << SystemStates[i].totAsk << " AskForced: " << SystemStates[i].totAskForced;
		LOG << "Bid " << SystemStates[i].totBid << " BidForced: " << SystemStates[i].totBidForced;
		cout << endl;
	}


}


// startSimulation ed endSimulation aggiornano a inizio e fine di ogni Run()
void TvBattery::startSimulation(){
	this->Payoff = 0.0;
	this->Energy = STARTEnergy;
	this->ResidualLife = STARTResidualLife;

	while (BatteryHistory.size() != 0) {
		BatteryHistory.erase(BatteryHistory.begin());
	}
	while (SystemStates.size() != 0) {
		SystemStates.erase(SystemStates.begin());
	}
	
}

void TvBattery::endSimulation() {}


bool  TvBattery::isStorage() {
	return true;
}

// plot
void TvBattery::DumpHtml(string path) {
	
	
	//blackoutIf(this->Threshold,false, true);
	//LOG << "<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>  "<<PotentialStates.size();
	//for (int i = 0; i < PotentialStates.size(); i++) {
		//LOG << PotentialStates[i];
	//}
	

	ostringstream oss;
	oss << path << "/battery" << Id << ".html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	// chiamo le funzioni per visualizzare i grafici
	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;
	fileO << "google.setOnLoadCallback(drawResidualLife);" << endl;
	fileO << "google.setOnLoadCallback(drawThresholdEvolution);" << endl;
	fileO << "google.setOnLoadCallback(drawPotentialStates);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawBidAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energia data ');" << endl;
	fileO << " data.addColumn('number', 'energia presa');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << max(-BatteryHistory[i].Transfert,0) << "," << max(BatteryHistory[i].Transfert,0) << "]," << endl;
		// BatteryHistory[t].Transfert è negativo -> Bid
		// BatteryHistory[t].Transfert è positivo -> Ask
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawResidualLife
	fileO << "function drawResidualLife() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'residualLife');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].ResidualLife << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ResidualLife'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawThresholdEvolution
	fileO << "function drawThresholdEvolution() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'threshold');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < ThresholdEvolution.size(); i++) {
		fileO << "      [" << i << "," << ThresholdEvolution[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ThresholdEvolution'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawPotentialStates
	//fileO << "function drawPotentialStates() {" << endl;
	//fileO << " var data = new google.visualization.DataTable();" << endl;
	//fileO << " data.addColumn('number', 'X');" << endl;
	//fileO << " data.addColumn('number', 'states');" << endl;
	//fileO << " data.addRows([" << endl;
	//for (int i = 0; i < PotentialStates.size(); i++) {
	//	fileO << "      [" << i << "," << PotentialStates[i] << "]," << endl;
	//}
	//fileO << "  ]);" << endl;
	//fileO << "  var options = {" << endl;
	//fileO << "    hAxis: {" << endl;
	//fileO << "     title: 'Time'" << endl;
	//fileO << "   }," << endl;
	//fileO << "    vAxis: {" << endl;
	//fileO << "       title: ' y ' " << endl;
	//fileO << "    }" << endl;
	//fileO << "   };" << endl;
	//fileO << "  var chart = new google.visualization.LineChart(document.getElementById('PotentialStates'));" << endl;
	//fileO << "   chart.draw(data, options);" << endl;
	//fileO << " }" << endl;


	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;
	fileO << "		<div id=\"ResidualLife\"></div>" << endl;
	fileO << "		<div id=\"ThresholdEvolution\"></div>" << endl;
	//fileO << "		<div id=\"PotentialStates\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

void TvBattery::DumpDat(string path, int rep){
	string pathFinal;
	if (rep == -1) {
		pathFinal = path.append("/EvolutionSingle.csv");
	}else {
		ostringstream oss;
		oss << path << "/EvolutionSingle" << rep << ".csv";
		pathFinal = oss.str();	
	}
	ofstream fileO(pathFinal.c_str());
	fileO << "time" << ",\t" << "threshold" << ",\t" << "stdDev" << ",\t" << "thresholdES" << ",\t" << "StdDevES" << endl;
	for (int i = 0; i < ThresholdEvolution.size(); i++) {
		fileO << i*updateInterval << ",\t" << ThresholdEvolution[i] << ",\t" << StdDevEvolution[i]<<",\t" << ThresholdESEvolution[i] << ",\t" << StdDevESEvolution[i] << endl;
	}
	fileO.close();
}

void TvBattery::changeThresholdTo(double newThreshold) {
	this->Threshold = newThreshold;
}

double TvBattery::getStdDevAt(int t) {
	return StdDevEvolution[t];
}