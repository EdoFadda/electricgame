// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <list>
#include "Battery.h"
#include "EvoBattery.h"
#include "EnergyBattery.h"
#include "WiseBattery.h"
#include "StdBattery.h"
#include "Building.h"
#include "NuclearPlant.h"
#include "RenewablePlant.h"
#include "AdaptiveBattery.h"
#include "Simulator.h"
#include "TVBattery.h"
#include <fstream>
#include <direct.h>


#define ASKINDEX 0
#define BIDINDEX 1
#define ASKFORCEDINDEX 2
#define BIDFORCEDINDEX 3

Simulator::Simulator(vector<Player*>& p){
	this->players = p;
	// metto a tutte le wise Battery la lista dei player
	for (auto pl : players) {
		if ((dynamic_cast<WiseBattery*>(pl) != NULL)) {
			((WiseBattery*)pl)->setPlayers(p);
		}
	}
}

int Simulator::firstPowerOutage(){
	for (int i = 0; i < historyStates.size(); i++) {
		if (historyStates[i].blackOut && i > historyStates.size()*0.1)
			return i;
	}
	return historyStates.size();
}

bool Simulator::happensBlackOut(){
	//bool flag = false;

	//bool ceTVbattery = false;
	//for (auto p : players)
	//	if ((dynamic_cast<TvBattery*>(p) != NULL))
	//		ceTVbattery = true;
		
	//for (int i = 0; i < historyStates.size(); i++) {
	//	if (historyStates[i].blackOut && i > historyStates.size()*0.2) {
	//		if (ceTVbattery) {
	//			for (auto p : players) {
	//				if ((dynamic_cast<TvBattery*>(p) != NULL)) {
	//					if (((TvBattery*)p)->getStdDevAt(i) < 0.5) {
	//						flag = true;
	//						LOG << i << " " << ((TvBattery*)p)->getStdDevAt(i);
	//					}
	//				}
	//			}
	//		}
	//		else {
	//			flag = true;
	//		}
	//	}
	//}
	//return flag;

	for (int i = 0; i < historyStates.size(); i++) {
		if (historyStates[i].blackOut && i > historyStates.size()*0.5)
			return true;
	}
	return false;

}


void Simulator::Run(int T, bool verbose) {
	while(historyStates.size() != 0){
		historyStates.erase(historyStates.begin());
	}
	
	for (auto p : players) {
		p->startSimulation();
	}

	int nBlackOut = 0;
	int nNormal = 0;
	int nDifficult = 0;
	int nUtopy = 0;
	

	if (verbose) { LOG << "Simulation start";}

	systemState* frozen = new systemState[T];
	
	
	int count = 0;
	
	// Inizializzo le offerte
	double** playerOffers = new double*[players.size()];
	for (int i = 0; i < players.size(); i++) {
		playerOffers[i] = new double[4]; // in ordine sono Ask, Bid, Ask forzato e Bid forzato.
		for (int j = 0; j < 4; j++) {
			playerOffers[i][j] = 0.0;
		}
	}


	for(auto rep=0; rep < T; rep++) {

		frozen[rep] = { false,false,0.0,0.0,0.0,0.0 };
		
		if (verbose) {
			LOG << "\n";
			LOG << "### Start of day " << rep + 1;
			for (auto p : this->players) {
				if (p->isStorage()) {
					LOG << "\t Energy Battery: " << ((Battery*)p)->getEnergy();
				}
			}
		}
		
		bool blackOut = false;
		bool normal = false;
		bool difficult = false;
		bool utopy = false;

		
		
		count = 0;
		for (auto p: this->players){
			double a = p->Ask(frozen[rep], true); // ask forzato: 2
			double b = p->Bid(frozen[rep], false);// bid normale: 1
			double c = p->Bid(frozen[rep], true); // bid forzato: 3
			double d = p->Ask(frozen[rep], false); // ask non forzato: 0
			
			playerOffers[count][ASKFORCEDINDEX] = a;
			playerOffers[count][BIDINDEX] = b;
			playerOffers[count][BIDFORCEDINDEX] = c;
			playerOffers[count][ASKINDEX] = d;
			
			frozen[rep].totAskForced += a;
			frozen[rep].totBid += b;	
			frozen[rep].totBidForced += c;
			frozen[rep].totAsk += d;
			count++; 
        }

		if (verbose) { LOG << "### TotBid:" << frozen[rep].totBid << " TotAskForced: " << frozen[rep].totAskForced; }
		
				//for (int i = 0; i < players.size(); i++) {
				//	for (int j = 0; j < 4; j++) {
				//		cout << playerOffers[i][j] << "\t";
				//	}
				//	cout << endl;
				//}

		// CLASSIFICO GLI STATI:
		if (frozen[rep].totAskForced > frozen[rep].totBid) { // se c'è più domanda
			if (frozen[rep].totBidForced < frozen[rep].totAskForced){
				blackOut = true;
				frozen[rep].blackOut = true;
				nBlackOut++;
			}else {
				difficult = true;
				frozen[rep].danger = true;
				nDifficult++;
			}
		}else{ // se c'è più offerta
			if (frozen[rep].totAsk <= frozen[rep].totBid){
				utopy = true;
				frozen[rep].utopy = true;
				nUtopy++;
			}else {
				normal = true;
				frozen[rep].normal = true;
				nNormal++;
			}
		}
		

		// Transition:
		if (blackOut) {
			if (verbose) { LOG << "### BLACKOUT! TotBidForced:" << frozen[rep].totBidForced << " TotAskForced: " << frozen[rep].totAskForced; }
			count = 0;
			
			// calcolo fromNonStorage
			double fromNonStorage = 0.0;
			for (auto p : players) {
				if (!p->isStorage()) {
					fromNonStorage += playerOffers[count][BIDFORCEDINDEX];
				}
				count++;
			}

			count = 0;
			for (auto p : players) {
				if ((dynamic_cast<AdaptiveBattery*>(p) != NULL)) {
					((AdaptiveBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				if ((dynamic_cast<TvBattery*>(p) != NULL)) {
					((TvBattery*)p)->addFromNonStorage(fromNonStorage);
				}

				p->Punishment();
				if (p->isStorage()){
					p->Transfer(frozen[rep], -((Battery*)p)->getEnergy());
				}else{
					(playerOffers[count][ASKFORCEDINDEX] >= 0) ? p->Transfer(frozen[rep], +playerOffers[count][ASKFORCEDINDEX]) : 0;
					(playerOffers[count][BIDFORCEDINDEX] > 0) ? p->Transfer(frozen[rep], -playerOffers[count][BIDFORCEDINDEX]) : 0;
				}
				count++;
			}
		}
		
		if (utopy) {
			if (verbose) { LOG << "### Utopy TotBid:" << frozen[rep].totBid << " TotAsk: " << frozen[rep].totAsk; }
			count = 0;
			// calcolo fromNonStorage
			double fromNonStorage = 0.0;
			for (auto p : players) {
				if (!p->isStorage()) {
					fromNonStorage += playerOffers[count][BIDFORCEDINDEX];
				}
				count++;
			}
		
			
			count = 0;
			// Trasferisco
			for (auto p : players){
				if ((dynamic_cast<AdaptiveBattery*>(p) != NULL)) {
					((AdaptiveBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				if ((dynamic_cast<TvBattery*>(p) != NULL)) {
					((TvBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				//LOG << playerOffers[count][0] << " -- " << playerOffers[count][1] << " -- " << playerOffers[count][2] << " -- " << playerOffers[count][3];
				(playerOffers[count][ASKINDEX] >= 0) ? p->Transfer(frozen[rep], +playerOffers[count][ASKINDEX]) : 0;
				(playerOffers[count][BIDINDEX] > 0) ? p->Transfer(frozen[rep], -playerOffers[count][BIDINDEX]) : 0;
				count++;
			}

		}

		if (normal) {
			if (verbose) { LOG << "### Normal TotBid:" << frozen[rep].totBid << " TotAsk: " << frozen[rep].totAsk; }
			// vuol dire che non soddisfo tutta la domanda non forzata ma una buona parte
			// quindi soddisfo tutti nella stessa percentuale
			
			double fromNonStorage = 0.0;
			double* transfert = new double[players.size()];
			
			// inizio a mettere gli ask forced
			count = 0;
			for (auto p : players) {
				transfert[count] = 0.0;
				// if the player is a bidder I set it in the offers:
				(playerOffers[count][BIDINDEX] > 0) ? transfert[count] = -playerOffers[count][BIDINDEX] : 0;
				// inizio a soddisfare la richiesta obbligatoria
				(playerOffers[count][ASKFORCEDINDEX] >= 0) ? transfert[count] = playerOffers[count][ASKFORCEDINDEX] : 0;
				// calcolo fromNonStorage
				if (!p->isStorage()) {
					fromNonStorage += playerOffers[count][BIDFORCEDINDEX];
				}
				count++;
			}


			double percEnergyFullfilled = (frozen[rep].totBid - frozen[rep].totAskForced) / (frozen[rep].totAsk - frozen[rep].totAskForced);
			
			//LOG << "------------------------------------";
			//LOG << "--rimane: " << frozen[rep].totBid - frozen[rep].totAskForced;
			//LOG << "--da soddisfare: " << (frozen[rep].totAsk - frozen[rep].totAskForced);
			//LOG << "--percentuale: " << percEnergyFullfilled;
			
			// adesso metto il resto:
			//  percEnergyFullfilled*(playerOffers[count][0] - playerOffers[count][2]) : 0; // se ask forzato è diverso da zero inizio a metterlo

			count = 0;
			for (auto p : players) {
				(playerOffers[count][ASKINDEX] > playerOffers[count][ASKFORCEDINDEX]) ? transfert[count] += percEnergyFullfilled*(playerOffers[count][ASKINDEX]-playerOffers[count][ASKFORCEDINDEX]): 0;
				//LOG << count << " --> " << transfert[count];
				count++;
			}

			count = 0;
			for (auto p : players) {
				if ((dynamic_cast<AdaptiveBattery*>(p) != NULL)) {
					((AdaptiveBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				if ((dynamic_cast<TvBattery*>(p) != NULL)) {
					((TvBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				p->Transfer(frozen[rep], transfert[count]);
				count++;
			}
		}
		
		if (difficult) {
			// vuol dire che soddisfo la domanda solo nel caso in cui l'offerta si sforza
			// quindi prendo da tutti nella stessa percentuale
			if (verbose) { LOG << "### Difficult TotBidForced:" << frozen[rep].totBidForced << " TotAskForced: " << frozen[rep].totAskForced; }
			double* transfert = new double[players.size()];

			// inizio a mettere i bid dalle fonti non storage
			double fromNonStorage = 0.0;
			count = 0;
			for (auto p : players) {
				transfert[count] = 0.0;
				// if the player is a nonStorage bidder I set it in the offers:
				if (!p->isStorage()) {
					transfert[count] = -playerOffers[count][BIDFORCEDINDEX];
					fromNonStorage += playerOffers[count][BIDFORCEDINDEX];
				}
				// if the player is an asker i set transfer to the right amount
				(playerOffers[count][ASKFORCEDINDEX] >= 0) ? transfert[count] = playerOffers[count][ASKFORCEDINDEX] : 0;
				count++;
			}


			double percEnergyTaken = (frozen[rep].totAskForced - fromNonStorage) / (frozen[rep].totBidForced - fromNonStorage);

			//LOG << frozen[rep].totBid - frozen[rep].totAskForced <<" -- "<< (frozen[rep].totAsk - frozen[rep].totAskForced);

			//cout << "-------> " << percEnergyTaken;
			count = 0;
			for (auto p : players) {
				if (p->isStorage()) {
					transfert[count] += -playerOffers[count][BIDFORCEDINDEX]*percEnergyTaken;
				}

				//(playerOffers[count][0] > playerOffers[count][2]) ? transfert[count] += percEnergyFullfilled*(playerOffers[count][0] - playerOffers[count][2]) : 0;
				//LOG << count << " --> " << transfert[count];
				count++;
			}

			count = 0;
			for (auto p : players){
				if ((dynamic_cast<AdaptiveBattery*>(p) != NULL)) {
					((AdaptiveBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				if ((dynamic_cast<TvBattery*>(p) != NULL)) {
					((TvBattery*)p)->addFromNonStorage(fromNonStorage);
				}
				p->Transfer(frozen[rep], transfert[count]);
				count++;
			}

		}


		if (verbose) {
			for (auto p : this->players) {
				if (p->isStorage()) {
					LOG << "\t Energy Battery: " << ((Battery*)p)->getEnergy();
				}
			}
			LOG << "### End of day " << rep + 1 << "\n\n"; 
		}
		
		historyStates.push_back(frozen[rep]);
    }



	// Dealloco la memoria
	for (int i = 0; i < players.size(); i++) {
		delete playerOffers[i];
	}
	delete playerOffers;
	playerOffers = NULL;
	delete frozen;


	if (verbose) { LOG << "### Simulation end"; }
	for (auto p : players) {
		p->endSimulation();
	}


	if (verbose){
		LOG << "### nBlackOut:" << nBlackOut;
		LOG << "### nNormal:" << nNormal;
		LOG << "### nDifficult:" << nDifficult;
		LOG << "### nUtopy:" << nUtopy;
	}

	
}




void Simulator::UpdateBatteriesThreshold(int version, bool verbose, int nVite){
	// imposto alle batterie il tipo di aggiornamento
	int count = 0;
	if ((version == DETEVO)|| (version == STOCEVO)) {
		for (auto p : players) {
			if ((dynamic_cast<EnergyBattery*>(p) != NULL)) {
				LOG << "NEW THRESHOLD FUNCTION OF BATTERY " << count;
				((EnergyBattery*)p)->newThreshold(version,verbose, nVite);
				count++;
			}
		}
		LOG << "###############################################";
		LOG << "###############################################";
	}else {
		LOG << "Error in version value";
		exit(1);
	}
	
}

void Simulator::generateGeography(double minX, double maxX, double minY, double maxY, double percBorder ){
	//s.generateGeography(-10,10,-10,10,0.2);
	//s.dumpGeograpy("C:/Users/edofa/Documents/BitBucket/electricgames/outputHtml");
	auto x = 0.0;
	auto y = 0.0;
	static random_device r;
	static mt19937 gen{ r() };

	for (auto p : this->players) {

		// the production centers are in the border
		if ((dynamic_cast<NuclearPlant*>(p) != NULL)||(dynamic_cast<RenewablePlant*>(p) != NULL)) {
			std::bernoulli_distribution d(0.5);
			auto up = d(gen);
			auto left = d(gen);
			if (up) {
				std::uniform_real_distribution<double> xd(maxX-percBorder*abs(maxX-minX),maxX);
				x = xd(gen);
			} else {
				std::uniform_real_distribution<double> xd(minX, minX + percBorder*abs(maxX - minX));
				x = xd(gen);
			}
			if (left) {
				std::uniform_real_distribution<double> yd(maxY - percBorder*abs(maxY - minY), maxY);
				y = yd(gen);
			} else {
				std::uniform_real_distribution<double> yd(minY, minY + percBorder*abs(maxY - minY));
				y = yd(gen);
			}	
		}

		// the building positions are in the center
		if ((dynamic_cast<Building*>(p) != NULL)||(dynamic_cast<Battery*>(p) != NULL)) {
			std::uniform_real_distribution<double> xd(minX + percBorder*abs(maxX - minX), maxX - percBorder*abs(maxX - minX));
			x = xd(gen);
			std::uniform_real_distribution<double> yd(minY + percBorder*abs(maxY - minY), maxY - percBorder*abs(maxY - minY));
			y = yd(gen);
		}
		p->setPosition(x, y);
	}

}

void Simulator::dumpResult(string path, int count) {

	this->stdProduction = 0;
	for (int i = 0; i < players.size(); i++) {
		if (dynamic_cast<NuclearPlant*>(players[i]) != NULL) {
			stdProduction += ((NuclearPlant*)players[i])->getStdProduction(); // stdProduction is constant in each states
		}
	}
	ostringstream oss;
	
	if(count != -1){
		oss << path << "/HistoryResults_" << count << ".html";
	}
	else {
		oss << path << "/HistoryResults.html";
	}
	//LOG << oss.str().c_str();
	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "google.setOnLoadCallback(drawScenario);" << endl;
	fileO << "google.setOnLoadCallback(drawMarket);" << endl;
	fileO << "google.setOnLoadCallback(drawStates);" << endl;

	// Disegna Scenario
	fileO << "function drawScenario() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'stdBid ');" << endl;
	fileO << " data.addColumn('number', 'Bid ');" << endl;
	fileO << " data.addColumn('number', 'ForcedBid ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < historyStates.size(); i++) {
		fileO << "      [" << i << "," << stdProduction << "," << historyStates[i].totBid << "," << historyStates[i].totBidForced << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Scenario'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// Disegna Market
	fileO << "function drawMarket() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'Ask ');" << endl;
	fileO << " data.addColumn('number', 'ForcedAsk ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < historyStates.size(); i++) {
		fileO << "      [" << i << "," << historyStates[i].totAsk << "," << historyStates[i].totAskForced << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Market'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// Disegna States
	fileO << "function drawStates() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'states ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < historyStates.size(); i++) {
		if(historyStates[i].blackOut) fileO << "      [" << i << "," << 0 << "]," << endl;
		if (historyStates[i].danger) fileO << "      [" << i << "," << 1 << "]," << endl;
		if (historyStates[i].normal) fileO << "      [" << i << "," << 2 << "]," << endl;
		if (historyStates[i].utopy) fileO << "      [" << i << "," << 3 << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('States'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// carica una pagina
	for (auto p : this->players) {
			if (dynamic_cast<Battery*>(p) != NULL) {
				fileO << "function loadBattery" << p->getId() << "(){" << endl;
			}
			else if (dynamic_cast<Building*>(p) != NULL) {
				fileO << "function loadBuilding" << p->getId() << "(){" << endl;
			}
			else if (dynamic_cast<NuclearPlant*>(p) != NULL) {
				fileO << "function loadNuclearPlant" << p->getId() << "(){" << endl;
			}
			
			fileO << "	document.getElementById(\"content\").innerHTML='<object type=\"text/html\" data=";
			if (dynamic_cast<Battery*>(p) != NULL) {
				fileO<<" \"players/battery";
			}else if (dynamic_cast<Building*>(p) != NULL) {
				fileO << " \"players/building";
			}else if (dynamic_cast<NuclearPlant*>(p) != NULL) {
				fileO << " \"players/nuclearPlant";
			}
			fileO<< p->getId() << ".html\" style=\"width: 100%;height: 100%\"></object>';" << endl;
			fileO << "} " << endl;
	}

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "	<div>" << endl;
	fileO << "		Bid e Ask sono negativi in caso di black out" << endl;
	fileO << "		<div style = \"float: left; width: 50%\" id=\"Scenario\"></div>" << endl;
	fileO << "		<div style = \"float: right; width: 50%\" id=\"Market\"></div>" << endl;
	fileO << "		<div style = \"float: right; width: 50%\" id=\"States\"></div>" << endl;
	fileO << "	</div>" << endl;

	// States
	

	// Battery

	fileO << "	<div id=\"topBar\"> " << endl;

	for (auto p : this->players) {
		if(dynamic_cast<Battery*>(p) != NULL){
			fileO << "		<a href=\"#\" onclick=\"loadBattery" << p->getId() << "()\"> Battery" << p->getId() << "</a>" << endl;
		}else if (dynamic_cast<Building*>(p) != NULL) {
			fileO << "		<a href=\"#\" onclick=\"loadBuilding" << p->getId() << "()\"> Building" << p->getId() << "</a>" << endl;
		}else if (dynamic_cast<NuclearPlant*>(p) != NULL){
			fileO << "		<a href=\"#\" onclick=\"loadNuclearPlant" << p->getId() << "()\"> NuclearPlant" << p->getId() << "</a>" << endl;
		}
	}

	fileO << "	</div>" << endl;

	fileO << "	<div id=\"content\" style=\"width: 100%; height: 100%; border: 3px solid #73AD21;\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
	_mkdir(path.append("/players").c_str());
	for (auto p : this->players) {
		p->DumpHtml(path);
	}
}

void Simulator::dumpGeograpy(string path) {

	ofstream fileO(string(path).append("/Geography.html").c_str());
	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>" << endl;
	fileO << "<script type=\"text/javascript\">" << endl;
	fileO << "	google.charts.load('current', {'packages':['corechart']});" << endl;
	fileO << "	google.charts.setOnLoadCallback(drawChart);" << endl;
	fileO << "	function drawChart() {" << endl;
	fileO << "		var data = new google.visualization.DataTable();" << endl;
	fileO << "		data.addColumn('number', 'X');" << endl;
	fileO << "		data.addColumn('number', 'Y');" << endl;
	fileO << "		data.addColumn({'type': 'string', 'role': 'style'});" << endl;
	fileO << "		data.addRows([" << endl;
	for (auto p : this->players){
		if ((dynamic_cast<NuclearPlant*>(p) != NULL) || (dynamic_cast<RenewablePlant*>(p) != NULL)) {
			fileO << "			[" << p->getX() << ",      " << p->getY() << " , 'point { size: 18; shape-type: square; fill-color: #0000ff; }'" << "],//" << p->getId() << endl;
			fileO << "			[null,      null , null" << "]," << endl;
		}
		if (dynamic_cast<Building*>(p) != NULL) {
			fileO << "			[" << p->getX() << ",      " << p->getY() << " , 'point { size: 18; shape-type: circle; fill-color: #ff0000; }'" << "],//" << p->getId() << endl;
			fileO << "			[null,      null , null" << "]," << endl;
		}
	}
	fileO << "		]);" << endl;
	fileO << "		var options = {" << endl;
	fileO << "			legend: 'none'," << endl;
	fileO << "			curveType: 'function'," << endl;
	fileO << "			pointSize: 7," << endl;
	fileO << "			series: {0: {lineWidth: 1}}" << endl;
	fileO << "		};" << endl;

	fileO << "		var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));" << endl;
	fileO << "		chart.draw(data, options);" << endl;
	fileO << "	}" << endl;
	fileO << "</script>" << endl;
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	fileO << "<div id=\"chart_div\" style=\"width: 900px; height: 500px;\"></div>" << endl;
	fileO << "</body>" << endl;
	fileO << "</html>" << endl;

}

void Simulator::plotXYGraph(vector<double>& x, vector<double>& y, string path, int count) {
	// dato un vettore x e un vettore y di lungezza size la funzione stampa un grafico xy.
	int size = x.size();
	ostringstream oss;
	oss<< path << "/graphXY_" << count<< ".html";
	
	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;
	fileO << "google.setOnLoadCallback(drawBasic0);" << endl;
	fileO << "function drawBasic0() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'PayOff ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < size; i++) {
		fileO << "      [" << x[i] << "," << y[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'x'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('speedProfile0'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	fileO << "  <div id=\"speedProfile0\"></div>" << endl;
	fileO << "</body>" << endl;
	fileO << "</html>" << endl;

	fileO.close();
}

void Simulator::plotXYGraphTXT(vector<double>& x, vector<double>& y, string path){
	int size = x.size();
	ostringstream oss;
	oss << path << "/data.txt";

	ofstream fileO(oss.str().c_str());
	fileO <<"data=[  " << endl;
	for (int i = 0; i < size; i++) {
		fileO << x[i] << ",  " << y[i] << ";" << endl;
	}
	fileO << "] " << endl;
	fileO << "x = data(:, 1)" << endl;
	fileO << "y = data(:, 2)" << endl;
	fileO << "plot(x,y)" << endl;

	fileO.close();
}

void Simulator::trainBatteries(int nViteTraining, int durataVite, bool verbose) {
	for (int j = 0; j < nViteTraining; j++) {
		this->Run(durataVite);
	}
}

double Simulator::maxAsk() {
	double ris = 0;
	for (int i = 0; i < historyStates.size(); i++)
		if (historyStates[i].totAskForced > ris)
			ris = historyStates[i].totAskForced;
	return ris;
}
