#ifndef C_PARTICLE_H
#define C_PARTICLE_H

#include "swarm.h"
#include "objectiveFunction.h"

class cParticle
{

private:
	int life;

	int inputDim;           // Number of variables to be optimized
	double* maxValues;
	double* minValues;

	// actual values
	double OF;
    double* position;
	
	// individual best values
	double bestOF_individual;
	double* bestPosition_individual;
	
	// collective best values
	double bestOF_global;
	double* bestPosition_global;

	objectiveFunction* ObjFunc;
    double* speed;

	// coefficients:
	double omega;
	double phi_ind;
	double phi_glb;

public:
	
	cParticle() {};
    cParticle(int inputDim, double* minValues, double* maxValues, int MAXEPOCHS, objectiveFunction* ObjFunc, double omega, double phi_ind, double phi_glb);
	

	// Actual Values
	double getOF();
    void getPosition(double* pos);

	// Best Values
    double getBestOF();
	void getBestPosition(double* pos);

	void copyGlobalOptimalPositionFrom(cParticle* p);
	void setBestGlobalPosition(double* pos);

	void updatePosition();
	void print();

}; 

#endif