// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "StdBattery.h"

using namespace std;



// methods to interact with the world
double StdBattery::Ask(systemState s, bool forced){
	if (ResidualLife == 0) {
		return 0.0;
	}
	if (Brain(forced) > 0) {
		return Brain(forced);
	}else {
		return 0.0;
	}

}

double StdBattery::Bid(systemState s, bool forced){
	if (ResidualLife == 0) {
		return 0.0;
	}
	if (Brain(forced) < 0) {
		return -Brain(forced);
	}else {
		return 0.0;
	}
}

void StdBattery::Transfer(systemState s, double amount) {
	Energy += amount;
	HistoryEnergy.push_back(Energy);
	if (amount >= 0) {
		HistoryAsk.push_back(amount);
		HistoryBid.push_back(0.0);
	}
	else {
		HistoryAsk.push_back(0.0);
		HistoryBid.push_back(-amount);
	}

	// Arrange life
	if (amount != 0) {
		ResidualLife--;
	}
	HistoryPayOffs.push_back(-pow(Brain(true) - amount, 2.0));
}


// output methods
double StdBattery::getEnergy(){
	return Energy;
}

// internal intelligence
double StdBattery::Brain(bool forced) {
	if (forced) {
		return -Energy;
	}else {
		if (Energy < Capacity) {
			return 10;
		}
		else {
			return 0;
		}
	}
}


// plot
void StdBattery::DumpHtml(string path){
	ostringstream oss;
	oss << path << "/battery" << Id << ".html";
	
	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < HistoryEnergy.size(); i++) {
		fileO << "      [" << i << "," << HistoryEnergy[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;
	// drawEnergy
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < HistoryAsk.size(); i++) {
		fileO << "      [" << i << "," << HistoryBid[i] << "," << HistoryAsk[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

void StdBattery::endSimulation(){
	return;
}

bool StdBattery::isStorage() {
	return true;
}