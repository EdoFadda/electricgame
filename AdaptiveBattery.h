// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_ADAPTIVEBATTERY_H
#define EG_ADAPTIVEBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <random>
#include "string"
#include "Player.h"
#include "Battery.h"



struct AdaptiveBatteryState {
	double ResidualLife;
	double Energy;
	double Transfert;
	double FromNonStorage;
	double Ask;
};

#define BLACK_OUT_CODE 0
#define DANGER_CODE 1
#define NORMAL_CODE 2
#define UTOPY_CODE 3

class AdaptiveBattery : public Battery {
public:

	AdaptiveBattery(double cap, double energy0, int life, double DeltaCharge, double DeltaDischarge, int updateInterval, double learningRate = 0.1, double precision = 0.01, double MAX_ITER = 100, double threshold = 0.0) : Battery(cap, energy0, life) {
		this->updateInterval = updateInterval;
		this->learningRate= learningRate;
		this->precision = precision;
		this->MAX_ITER = MAX_ITER;

		this->DeltaCharge = DeltaCharge;
		this->DeltaDischarge = DeltaDischarge;
		this->STARTEnergy = energy0;
		this->STARTResidualLife = life;
		this->Energy = energy0;
		this->minEnergy = energy0;
		if (energy0 == 0) {
			LOG << "Please set energy0 to be higher, otherwise the deterministic evolution of the threshold will suffer.";
		}
		this->Capacity = cap;
		this->ResidualLife = life;
		this->Threshold = threshold;
		this->presentLife = 0;
	}

	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);
	void Transfer(systemState s, double amount);


	void update(bool verbose = false);
	
	
	void startSimulation();
	void endSimulation();
	

	void printThreshold();
	void printHistory();
	void DumpHtml(string path);
	void DumpDat(string path, int rep=-1);

	void changeThresholdTo(double newThreshold);

	bool blackoutIf(double threshold, bool verbose = false, bool memo = false);	
	
	double getEnergy();
	double getEnergyThreshold();
	double getDeltaCharge();
	double getResidualLife();

	void addFromNonStorage(double fromNonStorage);

	double Brain(bool forced);

private:
	// technical data:
	
	int updateInterval;
	double learningRate;
	double precision;
	double MAX_ITER;

	double ResidualLife;
	double Energy;
	double Load;
	double Capacity;
	double DeltaDischarge;
	double DeltaCharge;

	double STARTEnergy;
	double STARTResidualLife;
	// decisional data LONG
	int presentLife;
	double Threshold;
	double BestEnergyThreshold;
	double minEnergy;

	double FromNonStorage;

	// HistoricalData:
	std::vector<AdaptiveBatteryState> BatteryHistory; // qua memorizzo la storia della Batteria
	std::vector<systemState>  SystemStates; // qua memorizzo la storia della Rete 
	std::vector<double> ThresholdEvolution;
	std::vector<double> PotentialStates;

	double sign(double x) {
		if (x > 0) 
			return 1;
		if (x < 0)
			return -1;
		else
			return 0;
	}

};

#endif //EG_BATTERY_H
