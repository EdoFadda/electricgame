// Filename: cParticle.cpp

#include "particle.h"

cParticle::cParticle(int inputDim, double* minValues, double* maxValues, int MAXEPOCHS, objectiveFunction* ObjFunc, double omega, double phi_ind, double phi_glb){
	this->inputDim = inputDim;
	this->omega = omega;
	this->phi_glb = phi_glb;
	this->phi_ind = phi_ind;
	this->ObjFunc = ObjFunc;
	this->position = new double[inputDim];
	this->minValues = new double[inputDim];
	this->maxValues = new double[inputDim];
	this->speed = new double[inputDim];
	this->bestPosition_individual = new double[inputDim];
	for (int i = 0; i < inputDim; i++) {
		this->minValues[i] = minValues[i];
		this->maxValues[i] = maxValues[i];
		// the firt position in random
		double rnd = (double)rand() / (double)RAND_MAX;
		this->position[i] = (maxValues[i]- minValues[i])*rnd + minValues[i];
		this->bestPosition_individual[i] = this->position[i];
		this->speed[i] = (double)rand() / (double)RAND_MAX;
	}
	life = 0;
	this->OF = ObjFunc->compute(this->position);
    this->bestOF_individual = OF;
	this->bestOF_global = OF;
}


double cParticle::getOF(){
	return this->OF;
}

void cParticle::getPosition(double* pos){
	for (int i = 0; i < inputDim; i++) {
		pos[i] = this->position[i];
	}
}

double cParticle::getBestOF(){
	return this->bestOF_individual;
}

void cParticle::getBestPosition(double* pos){
	for (int i = 0; i < inputDim; i++)
		pos[i] = this->bestPosition_individual[i];
	
}

void cParticle::copyGlobalOptimalPositionFrom(cParticle * p){
	double* pos = new double[inputDim];
	p->getBestPosition(pos);
	this->bestPosition_global = pos;
	this->bestOF_global = p->getOF();

}

void cParticle::setBestGlobalPosition(double * pos){
	for (int i = 0; i < inputDim; i++) 
		this->bestPosition_global[i] = pos[i];
}


void cParticle::updatePosition(){

	// calcolo la velocita':
	for (int i = 0; i < inputDim; i++) {
		double past_contr = speed[i];
		double best_ind_contr = (double)rand() / (double)RAND_MAX*(position[i] - bestPosition_individual[i]);
		double best_glb_contr = (double)rand() / (double)RAND_MAX*(position[i] - bestPosition_global[i]);
		speed[i] = omega*past_contr+ phi_ind*best_ind_contr+ phi_glb*best_glb_contr;
	}


	// aggiorno la posizione
	for (int i = 0; i < inputDim; i++) {
		this->position[i] += speed[i];
		// TODO: mettere a posto
		// se la posizione � fuori dai limiti la rimetto dentro a caso
		if (this->position[i] > maxValues[i]) {
			this->position[i] = (maxValues[i] - minValues[i])*(double)rand() / (double)RAND_MAX + minValues[i];
		}else if(this->position[i] < minValues[i]) {
			this->position[i] = (maxValues[i] - minValues[i])*(double)rand() / (double)RAND_MAX + minValues[i];
		}
	}
	OF = ObjFunc->compute(this->position);
	
	// se � meglio aggiorno la posizione best position individuale
	if (OF < bestOF_individual) {
		bestOF_individual = OF;
		for (int i = 0; i < inputDim; i++) {
			bestPosition_individual[i]=position[i];
		}
	}

	life++;
}


void cParticle::print() {

	for (int i = 0; i < inputDim; i++) 
		cout<< position[i] << "\t";

	cout << " ---> OF: " << OF << " || pBest:" << bestOF_individual << endl;
}