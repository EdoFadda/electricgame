// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_BATTERY_H
#define EG_BATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<vector>
#include "string"
#include "Player.h"
#include "Simulator.h"


struct batteryState {
	double Energy;
	double Ask;
	double Bid;
	double AskReal;
	double BidReal;
	double PayOff;
};


struct statistics {
	int ntimeStep;
	double sumPayoff;
	double sumSquarePayoff;
};

class Battery : public Player {
public:

	Battery(double cap, double energy0, int life ) : Player() {
		this->Energy = energy0;
		this->Capacity = cap;
		this->ResidualLife = life;
		HistoryEnergy.push_back(energy0);
	}
	
	virtual bool isStorage();

	virtual double Ask(systemState s, bool forced = false) = 0;
	virtual double Bid(systemState s, bool forced = false) = 0;
    
	virtual void Transfer(systemState s, double amount) = 0;
	virtual void DumpHtml(string path);
	virtual void endSimulation() { return; };
	virtual void startSimulation() { return; };

	

	virtual double getEnergy();

	virtual double Brain(bool forced) = 0;

private:
	int ResidualLife;
	double Energy;
    double Load;
    double Capacity;
	std::vector<double> HistoryEnergy;
	std::vector<double> HistoryAsk;
	std::vector<double> HistoryBid;
	std::vector<double> HistoryPayOffs;
};

#endif //EG_BATTERY_H
