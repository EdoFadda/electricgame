// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "WiseBattery.h"
#include "Building.h"
#include "NuclearPlant.h"
#include "RenewablePlant.h"

using namespace std;


# define CHARGE_CODE  1
# define DISCHARGE_CODE  -1




// methods to interact with the world
double WiseBattery::Ask(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf > 0) {
		ask = bf;
		bid = 0.0;
		return bf;
	}
	else {
		return 0.0;
	}

}

double WiseBattery::Bid(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(forced);
	if (Brain(forced) < 0) {
		ask = 0.0;
		bid = -bf;
		return -bf;
	}
	else {
		return 0.0;
	}
}


void WiseBattery::Transfer(systemState s, double amount) {
	// amount negativo -> ceduto energia
	// amount positivo -> acqistato energia
	if (ResidualLife == 0) {
		WiseBatteryState bS = { ResidualLife, Energy, 0.0, 0.0 ,0.0 ,0.0, 0.0, Payoff };
		BatteryHistory.push_back(bS);
		return;
	}


	Energy = min(Energy + amount, Capacity);
	SystemStates.push_back(s);

	// Arrange life
	
	if (BatteryHistory.size() == 0 ){
		ResidualLife--;
	}else{
		//if (sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) ) // se non hanno lo stesso segno allora
		//	ResidualLife--;
		if (amount > 0 && sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert)) {
			ResidualLife -=  ((int)Energy);
		}
	}
	
	// update payoff
	this->Payoff +=  max(ResidualLife, 0.0);


	// update BatteryHistory
	WiseBatteryState bS = {ResidualLife, Energy, amount, ask ,bid ,amount, 0.0, Payoff};
	BatteryHistory.push_back(bS);
	presentLife++;
	
}


// output methods
double WiseBattery::getEnergy() {
	return Energy;
}


// internal intelligence
double WiseBattery::Brain(bool forced) {
	// UNA VOLTA CHE A T-1 MI CARICO A T CERCO ANCORA DI CARICARMI
	if (forced){
		return -min(DeltaDischarge,Energy);
	}else{
		if (BatteryHistory[BatteryHistory.size() - 1].Transfert > 0) { // se all'istante prima mi stavo caricando continuo a farlo.
			return min(DeltaDischarge, Capacity);
		}

		int nScenarios = 10;
		
		// inizializzo gli scenari
		double** scenariosMatrix = new double*[nScenarios];
		for (int i = 0; i < nScenarios; i++) {
			scenariosMatrix[i] = new double[timeHorizonForecast];
			for (int t = 0; t < timeHorizonForecast; t++)
				scenariosMatrix[i][t] = 0.0;
		}
		
		// creo i vector per la descrizione delle altre batterie
		vector<double> thresholds;
		vector<double> energies;
		vector<double> deltaCharge;

		// acquisisco i dati:
		for (auto p : neighbors) {
			if ((dynamic_cast<Building*>(p) != NULL)) {
				((Building*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
			}else if ((dynamic_cast<EnergyBattery*>(p) != NULL)) {
				thresholds.push_back( ((EnergyBattery*)p)->getEnergy() );
				energies.push_back( ((EnergyBattery*)p)->getEnergyThreshold() );
				deltaCharge.push_back(((EnergyBattery*)p)->getDeltaCharge());
			}else if ((dynamic_cast<NuclearPlant*>(p) != NULL)) {
				((NuclearPlant*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
			}else if ((dynamic_cast<RenewablePlant*>(p) != NULL)) {
				((RenewablePlant*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
			}
		}
		
		//for (int i = 0; i < nScenarios; i++) {
		//	for (int t = 0; t < timeHorizonForecast; t++) {
		//		cout << scenariosMatrix[i][t] << "\t";
		//	}
		//	cout << endl;
		//}
		//cout << endl;
		//cout << "----------" << endl;
		//cout << endl;

		// scenari se scelgo di caricarmi  (dal tempo attulale fin quando riesco)
		double** scenariosMatrixC = new double*[nScenarios];
		for (int i = 0; i < nScenarios; i++) {
			scenariosMatrixC[i] = new double[timeHorizonForecast];
			for (int t = 0; t < timeHorizonForecast; t++)
				scenariosMatrixC[i][t] = scenariosMatrix[i][t];
		}

		// scenari se scelgo di non caricarmi sono semplicemente scenariosMatrix
		// TEST SIMULATE BATTERIES
		//simulateBatteries(scenariosMatrix[0], timeHorizonForecast, thresholds, energies, deltaCharge, false);
		

		for (int i = 0; i < nScenarios; i++) {
			simulateBatteries(scenariosMatrix[i], timeHorizonForecast, thresholds, energies, deltaCharge,false);
		}

		for (int i = 0; i < nScenarios; i++) {
			simulateBatteries(scenariosMatrixC[i], timeHorizonForecast, thresholds, energies, deltaCharge,true);
		}


		// VALUTO P(Black Out) e P(Non mi carico al massimo)
		double pBlackOutNC = computeBlackOutProbability(scenariosMatrix, nScenarios, timeHorizonForecast);
		double pBlackOutC = computeBlackOutProbability(scenariosMatrixC, nScenarios, timeHorizonForecast);
		double pNotFullCharge = computeNotFullChargeProbability(scenariosMatrix, nScenarios, timeHorizonForecast);
		//LOG << "Prob(NotFullCharge): " << pNotFullCharge;
		//LOG<< "Prob(Black Out): " << pBlackOutNC;

		// prendo la decisione
		if (tanh(atanh(pBlackOutC - pBlackOutNC) + Threshold*atanh(pNotFullCharge)) > 0.5) {
			return min(DeltaDischarge, Capacity);
		}else {
			return 0.0;
		}

	}
}



void WiseBattery::newThreshold(int version, bool verbose, int nVite) { // se version=DETEVO il prossimo thereshold sarà scelto in base alla storia, 
																		 // se version=STOCEVO il prossimo thereshold sarà scelto a caso.
																		 // we have finish a set of lives:
	if (bestStat.ntimeStep == -1) {
		if (verbose) {
			LOG << "Aggiorno perche' e' il primo giro, il nuovo BestEnergyThreshold e': " << Threshold << "  n Time Step: " << this->presentStat.ntimeStep;
			LOG << ""; LOG << "";
		}
		this->BestEnergyThreshold = Threshold;
		this->bestStat.ntimeStep = this->presentStat.ntimeStep;
		this->bestStat.sumPayoff = this->presentStat.sumPayoff;
		this->bestStat.sumSquarePayoff = this->presentStat.sumSquarePayoff;
		presentStat = { 0,0.0,0.0 };
		return;
	}


	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;
	double variancePayOff = presentStat.sumSquarePayoff / presentStat.ntimeStep - pow(meanPayOff, 2.0);
	if (verbose) { LOG << "PayOff: " << meanPayOff << " variance: " << variancePayOff << " n: " << presentStat.ntimeStep; }

	double meanBestPayOff = bestStat.sumPayoff / bestStat.ntimeStep;
	double varianceBestPayOff = bestStat.sumSquarePayoff / bestStat.ntimeStep - pow(meanBestPayOff, 2.0);
	if (verbose) { LOG << "Best PayOff: " << meanBestPayOff << " variance Best: " << varianceBestPayOff << " n: " << bestStat.ntimeStep; }


	double s = varianceBestPayOff / bestStat.ntimeStep + variancePayOff / presentStat.ntimeStep;

	double z = (meanPayOff - meanBestPayOff) / pow(s, 0.5);


	if (verbose) { LOG << " z test: " << z << " Quantile Thereshol" << QuantileThreshold; }

	// se il test mi dice che i nuovi sono migliori allora aggiorno il passato con il presente: 
	if (z > QuantileThreshold) {
		if (verbose) { LOG << "aggiorno perche' z: " << z << " maggiore di threshold: " << QuantileThreshold; }
		if (verbose) { LOG << "quindi il nuovo BestEnergyThreshold e': " << Threshold; }
		this->BestEnergyThreshold = Threshold;
		this->bestStat.ntimeStep = this->presentStat.ntimeStep;
		this->bestStat.sumPayoff = this->presentStat.sumPayoff;
		this->bestStat.sumSquarePayoff = this->presentStat.sumSquarePayoff;
	}
	if (verbose) { LOG << "Best threshold: " << BestEnergyThreshold; }

	if (version == DETEVO) {
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		//if (verbose) { LOG << "minEnergy: " << minEnergy; }
		//if (minEnergy <= 0) { // se il minimo dell'energia che ho è 0 allora aumento il threshold
		//	if (Threshold >= Capacity) {
		//		Threshold = BestEnergyThreshold;
		//	}
		//	Threshold = Threshold + varianceChangeThreshold*abs(d(gen));
		//}
		//else { // altrimenti lo diminuisco
		//	Threshold = Threshold - minEnergy;
		//}
		Threshold = min(Capacity, Threshold);
	}
	else if (version == STOCEVO) {
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		Threshold = Threshold + varianceChangeThreshold*d(gen);
		Threshold = min(Capacity, Threshold);
	}
	else if (version == SIMULATEDANEALING) {
		if (nVite == 0) {
			LOG << "nVite per la modalità SIMULATEDANEALING deve essere diverso da zero";
			exit(1);
		}
		static random_device r;
		static mt19937 gen{ r() };
		normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1
		Threshold = BestEnergyThreshold + Capacity*exp(-5.0*presentLife / nVite)*d(gen); // in questo modo quando presentLife == nVite uso una varianza del 6 per mille della capacità
		presentLife++;
		Threshold = ((int)Threshold) % ((int)Capacity);
	}
	else {
		LOG << "version non corretta";
		exit(1);
	}

	if (verbose) { LOG << "new Threshold: " << Threshold; LOG << ""; LOG << ""; }

	// e inizializzo le nuove statistiche:
	presentStat = { 0,0.0,0.0 };

}

void WiseBattery::setVarianceChangeThreshold(double value) {
	this->varianceChangeThreshold = value;
}




void WiseBattery::setHorizonForecast(int t){
	this->timeHorizonForecast = t;
}

void WiseBattery::startSimulation(){
	this->Payoff = 0.0;
	this->Energy = BatteryHistory[0].Energy;
	this->ResidualLife = BatteryHistory[0].ResidualLife;
	while (BatteryHistory.size() != 0) {
		BatteryHistory.erase(BatteryHistory.begin());
	}
	while (SystemStates.size() != 0) {
		SystemStates.erase(SystemStates.begin());
	}
	BatteryHistory.push_back({ this->ResidualLife, this->Energy, 0.0,0.0,0.0,0.0,0.0 });
}

void WiseBattery::endSimulation() {
	this->presentStat.ntimeStep++;
	this->presentStat.sumPayoff += this->Payoff;
	this->presentStat.sumSquarePayoff += pow(this->Payoff, 2.0);	
}


bool  WiseBattery::isStorage() {
	return true;
}

// plot
void WiseBattery::DumpHtml(string path) {
	ostringstream oss;
	oss << path << "/battery" << Id << ".html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	// chiamo le funzioni per visualizzare i grafici
	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;
	fileO << "google.setOnLoadCallback(drawResidualLife);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawBidAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Bid << "," << BatteryHistory[i].Ask << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawResidualLife
	fileO << "function drawResidualLife() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'residualLife');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].ResidualLife << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ResidualLife'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;
	fileO << "		<div id=\"ResidualLife\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}


void WiseBattery::printStatistics() {
	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;

	double variancePayOff = presentStat.sumSquarePayoff / presentStat.ntimeStep - pow(meanPayOff, 2.0);
	
	LOG<<"Threshold:"<< Threshold << "PayOff: " << meanPayOff << " variance: " << variancePayOff << " n: " << presentStat.ntimeStep;
}


double WiseBattery::getMeanPayOff() {
	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;
	return meanPayOff;
}