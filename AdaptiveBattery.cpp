// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "AdaptiveBattery.h"

using namespace std;


# define CHARGE_CODE  1
# define DISCHARGE_CODE  -1



double AdaptiveBattery::Ask(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf > 0) {
		return bf;
	}
	else {
		return 0.0;
	}

}

double AdaptiveBattery::Bid(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		return 0.0;
	}
	double bf = Brain(forced);
	if (bf < 0) {
		return -bf;
	}
	else {
		return 0.0;
	}
}


void AdaptiveBattery::Transfer(systemState s, double amount) {
	// amount negativo -> ceduto energia
	// amount positivo -> acqistato energia

	// Memorizzo lo stato del sistema
	SystemStates.push_back(s);

	// Arrange life
	if (BatteryHistory.size() == 0 ){
		ResidualLife--;
	}else{
		if (amount > 0 && sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert)) {
			ResidualLife -=  tanh(Energy) + 1.0;
		}
	}
	


	// update Energy 
	Energy = min(Energy + amount, Capacity);



	double Ask = 0.0;
	if (!s.blackOut && !s.danger) {
		if (Energy < Threshold) { // se ho meno energia del threshold allora continuo a caricare
			Ask = min(DeltaCharge, Capacity - Energy);
		}
		else {
			// se ho più energia del threshold allora 
			// al primo istante cerco subito di caricarmi
			if (BatteryHistory.size() == 0) {
				if (STARTEnergy < Threshold)
					Ask = min(DeltaCharge, Capacity - Energy);
				else
					Ask = 0.0;
			}
			else {
				// se l'istante prima mi stavo caricando continuo
				if (sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) == CHARGE_CODE) {
					Ask = min(DeltaCharge, Capacity - Energy);
				}
			}
		}
	}

	// update BatteryHistory
	AdaptiveBatteryState bS = { ResidualLife, Energy, amount, FromNonStorage, Ask };
	BatteryHistory.push_back(bS);


	if (presentLife % updateInterval == 0 && presentLife > 0) {
		//LOG << "UPDATE:  " << presentLife;
		this->update();
	}
	// update presentife
	presentLife++;
}

void AdaptiveBattery::addFromNonStorage(double fromNonStorage) {
	this->FromNonStorage = fromNonStorage;
}


// output methods
double AdaptiveBattery::getEnergy() {
	return Energy;
}

double AdaptiveBattery::getEnergyThreshold(){
	return this->Threshold;
}

double AdaptiveBattery::getDeltaCharge(){
	return this->DeltaCharge;
}

double AdaptiveBattery::getResidualLife(){
	return this->ResidualLife;
}

double AdaptiveBattery::Brain(bool forced) {
	
	if (forced) {
		return -min(DeltaDischarge, Energy);
	}
	else {
		if (Energy < Threshold) { // se ho meno energia del threshold allora continuo a caricare
			return min(DeltaCharge, Capacity - Energy);
		}
		else {
			// se ho più energia del threshold allora 
			// al primo istante cerco subito di caricarmi
			if (BatteryHistory.size() == 0) {
				if (STARTEnergy < Threshold)
					return min(DeltaCharge, Capacity - Energy);
				else
					return 0.0;
			}else {
				// se l'istante prima mi stavo caricando continuo
				if (sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) == CHARGE_CODE) {
					return min(DeltaCharge, Capacity - Energy);
				}
			}
		}
	}
}


bool AdaptiveBattery::blackoutIf(double threshold, bool verbose,bool memo){
	// return true if with threshold there would be a black out, false otherwise
	ofstream fileO("C:/Users/Edoardo/Desktop/log.txt");

	if (verbose) {
		//LOG << "Try with threshold " << threshold;
		fileO << "Try with threshold " << threshold << endl;
	}
	if(memo) PotentialStates.clear();
	

	vector<double> pastEnergy;
	bool chargeBefore = false;
	pastEnergy.push_back(STARTEnergy);
	pastEnergy.push_back(STARTEnergy);
	 

	for (int t = 1; t < SystemStates.size(); t++) {
		double Ask = 0.0;
		double ForcedBid = pastEnergy[t];
		double correzioneBid = -BatteryHistory[t - 1].Energy + pastEnergy[t];

		if (verbose){
			//LOG << "Time: " << t;
			//LOG << "AskForced(Bid): " << SystemStates[t].totAskForced<<" ("<< SystemStates[t].totBid<<")";
			//LOG << "BidForced: " << SystemStates[t].totBidForced - BatteryHistory[t].Energy + pastEnergy[t];
			fileO << "Time: " << t<<endl;
			fileO << "AskForced(Bid): " << SystemStates[t].totAskForced << " (" << SystemStates[t].totBid << ")" << endl;
			fileO << "BidForced: " << SystemStates[t].totBidForced - BatteryHistory[t].Energy + pastEnergy[t] << endl;
		}
		

		// CLASSIFICO GLI STATI:
		if (SystemStates[t].totAskForced > SystemStates[t].totBid) { // se c'è più domanda
			if (SystemStates[t].totBidForced + correzioneBid < SystemStates[t].totAskForced) {
				if (memo)
					PotentialStates.push_back(BLACK_OUT_CODE);
				else {
					//if(verbose) LOG << "BLACK OUT ";
					if (verbose) fileO << "BLACK OUT ";
					
					if (t > 0.3*presentLife || presentLife < 100) {
						return true;
					}else {
						pastEnergy.push_back(0.0);
						chargeBefore = false;
					}
				}
		}else {

			double perc = (SystemStates[t].totAskForced - BatteryHistory[t].FromNonStorage) / (SystemStates[t].totBidForced + correzioneBid - BatteryHistory[t].FromNonStorage);
			//LOG <<"--> " <<(SystemStates[t].totAskForced - BatteryHistory[t].FromNonStorage) << " " << (SystemStates[t].totBidForced + correzioneBid - BatteryHistory[t].FromNonStorage);
			if (memo) PotentialStates.push_back(DANGER_CODE);
				if (verbose) {
					//LOG << "DANGER";
					//LOG << "Initial Energy: " << pastEnergy[t];
					//LOG << "Perc: " << perc;
					//LOG << "Transfer: " << pastEnergy[t]*perc;
					//LOG << "Final Energy: " << pastEnergy[t]*(1.0 - perc);
					fileO << "DANGER" << endl;
					fileO << "Initial Energy: " << pastEnergy[t] << endl;
					fileO << "Perc: " << perc << endl;
					fileO << "Transfer: " << pastEnergy[t] * perc << endl;
					fileO << "Final Energy: " << pastEnergy[t] * (1.0 - perc) << endl;
				}
				pastEnergy.push_back(pastEnergy[t] * (1.0 - perc));
				chargeBefore = false;
			}
		}else { // se c'è più offerta
			if (pastEnergy[t] < threshold || chargeBefore) {
				Ask = min(DeltaCharge, Capacity - pastEnergy[t]);
			}

			if (SystemStates[t].totAsk - BatteryHistory[t].Transfert + Ask <= SystemStates[t].totBid) {
				if (memo) PotentialStates.push_back(UTOPY_CODE);
				if (verbose) {
					//LOG << "UTOPY" ;
					//LOG << "Initial Energy: " << pastEnergy[t]<<" ("<< threshold <<")";
					//LOG << "Ask: " << Ask;
					//LOG << "Final Energy: " << pastEnergy[t]+Ask << " (" << threshold << ")";
					fileO << "UTOPY" << endl;
					fileO << "Initial Energy: " << pastEnergy[t] << " (" << threshold << ")" << endl;
					fileO << "Ask: " << Ask << endl;
					fileO << "Final Energy: " << pastEnergy[t] + Ask << " (" << threshold << ")" << endl;
				}
				//pastEnergy.push_back(pastEnergy[t] + Ask);
				pastEnergy.push_back(pastEnergy[t] + Ask);
				if (Ask > 0)chargeBefore = true;
				else chargeBefore = false;
			}else {
				if (memo) PotentialStates.push_back(NORMAL_CODE);
				double perc = 0.0;
				double DeltaAsk = 0.0;
				if (Ask > 0) {
					DeltaAsk = Ask - BatteryHistory[t].Ask;
					// dentro perc c'è già contata la richiesta che ho fatto nel mondo realmente esistente
					perc = (SystemStates[t].totBid - SystemStates[t].totAskForced) / (DeltaAsk + SystemStates[t].totAsk - SystemStates[t].totAskForced);
				}


				if (verbose) {
					//LOG <<  "NORMAL";
					//LOG << "Initial Energy: " << pastEnergy[t];
					//LOG << "Percentage Satisfy (Ask): " << perc<<" ( "<<Ask<<" )";
					//LOG << "New Energy: " << pastEnergy[t] + perc*Ask;
					fileO << "NORMAL" << endl;
					fileO << "Initial Energy: " << pastEnergy[t] << endl;
					fileO << "Percentage Satisfy (Ask): " << perc << " ( " << Ask << " )" << endl;
					fileO << "New Energy: " << pastEnergy[t] + perc*Ask << endl;
				}
				pastEnergy.push_back(pastEnergy[t] + perc*Ask);
				if (Ask > 0) chargeBefore = true;
				else chargeBefore = false;
			}
		}
		if (verbose) {
			//cout << endl;
			fileO << endl;
		}
	}
	fileO.close();

	return false;
}

void AdaptiveBattery::update(bool verbose){
	
	double optimalThreshold = 0.0;
	// devo fare il rewind della storia della domanda e cercare il threshold ottimo
	
	// più è alto il threshold meno vita ho
	// più è basso il threshold più rischio il black-out
	// devo quindi trovare il minimo valore di threshold che non produce black-out
	double lower = 0.0;
	double upper = this->Capacity; // POSSO FARE UNA COSA PIU' FURBA CONSIDERANDO IL THRESHOLD ATTUALE
	

	// se non ho black out con zero -> nuovo threshold è zero
	if (!blackoutIf(lower)) {
		//blackoutIf(lower, true);
		optimalThreshold = lower;
		//cout << "exitL--";
	} // se ho black out con upper -> nuovo threshold è upper
	else if (blackoutIf(upper)) {
		//blackoutIf(upper, true);
		optimalThreshold = upper;
		//cout << "exitU--";
	}
	else {
		// START
		int iter=0;
		double medium = 0.0;

		while (abs(lower - upper) > 10 && iter < MAX_ITER) {
			medium = (lower + upper) / 2;
			if (verbose) {
				LOG << "upper: " << upper << "--->" << blackoutIf(upper);
				LOG << "lower: " << lower << "--->" << blackoutIf(lower);
				LOG << "medium: " << medium<<"--->"<< blackoutIf(medium);
				cout << endl;
			}

			// if medium produces a black out then lower = medium
			if (blackoutIf(medium)) {
				lower = medium;
			}// if medium does not produce black out then upper = medium
			else {
				upper = medium;
			}
			iter++;
		}
		optimalThreshold = upper;
	}
	
	
	// a questo punto aggiorno il threshold
	this->Threshold = min(this->Threshold + this->learningRate*(optimalThreshold - this->Threshold), this->Capacity);
	//this->Threshold = optimalThreshold ;

	//LOG << this->presentLife << " optimalThreshold: " << optimalThreshold<<" --> "<< Threshold;
}


void AdaptiveBattery::printThreshold() {
	LOG << this->BestEnergyThreshold;
}

void AdaptiveBattery::printHistory() {
	
	LOG << "Initial Condition";
	LOG << "Energy: " << STARTEnergy;
	cout << endl;

	for (int i = 0; i < BatteryHistory.size()-1; i++) {
		LOG << "Time " << i;
		LOG << "Energy(at the end of the time step): " << BatteryHistory[i].Energy;
		LOG << "Trans(during the time step): " << BatteryHistory[i].Transfert;
		LOG << "FNS: " << BatteryHistory[i].FromNonStorage;
		
		
		if (SystemStates[i].blackOut) LOG << "BLACK OUT ";
		if (SystemStates[i].danger) LOG << "DANGER ";
		if (SystemStates[i].normal) LOG << "NORMAL ";
		if(SystemStates[i].utopy) LOG << "UTOPY ";

		LOG << "Ask: " << SystemStates[i].totAsk << " AskForced: " << SystemStates[i].totAskForced;
		LOG << "Bid " << SystemStates[i].totBid << " BidForced: " << SystemStates[i].totBidForced;
		cout << endl;
	}


}


// startSimulation ed endSimulation aggiornano a inizio e fine di ogni Run()
void AdaptiveBattery::startSimulation(){
	this->Payoff = 0.0;
	this->Energy = STARTEnergy;
	this->ResidualLife = STARTResidualLife;

	while (BatteryHistory.size() != 0) {
		BatteryHistory.erase(BatteryHistory.begin());
	}
	while (SystemStates.size() != 0) {
		SystemStates.erase(SystemStates.begin());
	}
	
}

void AdaptiveBattery::endSimulation() {
	// aggiorno minEnergy:
	for (int i = 0; i < SystemStates.size(); i++) {
		if (BatteryHistory[i].Energy < minEnergy)
			minEnergy = BatteryHistory[i].Energy;
	}
}



bool  AdaptiveBattery::isStorage() {
	return true;
}

// plot
void AdaptiveBattery::DumpHtml(string path) {
	
	
	//blackoutIf(this->Threshold,false, true);
	//LOG << "<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>  "<<PotentialStates.size();
	//for (int i = 0; i < PotentialStates.size(); i++) {
		//LOG << PotentialStates[i];
	//}
	

	ostringstream oss;
	oss << path << "/battery" << Id << ".html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	// chiamo le funzioni per visualizzare i grafici
	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;
	fileO << "google.setOnLoadCallback(drawResidualLife);" << endl;
	fileO << "google.setOnLoadCallback(drawThresholdEvolution);" << endl;
	fileO << "google.setOnLoadCallback(drawPotentialStates);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawBidAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << max(-BatteryHistory[i].Transfert,0) << "," << max(BatteryHistory[i].Transfert,0) << "]," << endl;
		// BatteryHistory[t].Transfert è negativo -> Bid
		// BatteryHistory[t].Transfert è positivo -> Ask
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawResidualLife
	fileO << "function drawResidualLife() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'residualLife');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].ResidualLife << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ResidualLife'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawThresholdEvolution
	fileO << "function drawThresholdEvolution() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'threshold');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << ThresholdEvolution[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ThresholdEvolution'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawPotentialStates
	fileO << "function drawPotentialStates() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'states');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < PotentialStates.size(); i++) {
		fileO << "      [" << i << "," << PotentialStates[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('PotentialStates'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;
	fileO << "		<div id=\"ResidualLife\"></div>" << endl;
	fileO << "		<div id=\"ThresholdEvolution\"></div>" << endl;
	fileO << "		<div id=\"PotentialStates\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

void AdaptiveBattery::DumpDat(string path, int rep){
	string pathFinal;
	if (rep == -1) {
		pathFinal = path.append("/Evolution.dat");
	}else {
		ostringstream oss;
		oss << path << "/Evolution" << rep << ".dat";
		pathFinal = oss.str();	
	}
	ofstream fileO(pathFinal.c_str());
	fileO << "time" << "\t" << "threshold" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		if (i % updateInterval==0) {
			fileO << i << "\t" << ThresholdEvolution[i] << endl;
		}
	}
	fileO.close();
}



void AdaptiveBattery::changeThresholdTo(double newThreshold) {
	this->Threshold = newThreshold;
}
