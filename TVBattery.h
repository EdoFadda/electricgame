// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_TVBATTERY_H
#define EG_TVBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <random>
#include <limits>
#include "string"
#include "Player.h"
#include "Battery.h"



struct TvBatteryState {
	double ResidualLife;
	double Energy;
	double Transfert;
	double FromNonStorage; 
	double Ask;
};

#define BLACK_OUT_CODE 0
#define DANGER_CODE 1
#define NORMAL_CODE 2
#define UTOPY_CODE 3

#define FLAG_BLACKOUT -1527363.7364528
class TvBattery : public Battery {
public:

	TvBattery(double cap, double energy0, int life, double DeltaCharge, double DeltaDischarge, int updateInterval, int nGenerations = 60, double forgotRate = 0.01,
		double threshold = 500.0, double stdDev = -1, bool avoidBO = true, bool useDistr = false) : Battery(cap, energy0, life) {
		this->updateInterval = updateInterval;
		this->nGenerations = nGenerations;
		this->forgetRate = forgotRate;
		this->DeltaCharge = DeltaCharge;
		this->DeltaDischarge = DeltaDischarge;
		this->STARTEnergy = energy0;
		this->STARTResidualLife = life;
		this->Energy = energy0;
		this->Capacity = cap;
		this->ResidualLife = life;
		this->Threshold = threshold;
		this->presentLife = 0;
		if (stdDev <= 0){
			this->StdDev = Capacity/10;
		}else {
			this->StdDev = stdDev;
		}
		ThresholdEvolution.push_back(this->Threshold);
		StdDevEvolution.push_back(this->StdDev);
		ThresholdESEvolution.push_back(this->Threshold);
		StdDevESEvolution.push_back(this->StdDev);
		performanceThreshold = -DBL_MAX;
		TEMP = 1;
		this->useDistr = useDistr; 
		this->minThreshold = 0.0;
		this->ThresholdES = Threshold;
		this->StdDevES = StdDev;
		this->avoidBO = avoidBO;
	}

	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);

	void Transfer(systemState s, double amount);


	void update(bool verbose = false);
	void update_1fifth(bool verbose = false);
	
	void startSimulation();
	void endSimulation();
	

	void printThreshold();
	void printHistory();
	void DumpHtml(string path);
	void DumpDat(string path, int rep=-1);

	void changeThresholdTo(double newThreshold);

	double simulate(double threshold,double safety=0.0, bool verbose = false);	
	
	double getEnergy();
	double getThreshold();
	double getMinThreshold();
	double getStdDev();
	double getDeltaCharge();
	double getResidualLife();

	void addFromNonStorage(double fromNonStorage);

	double Brain(bool forced);
	double getStdDevAt(int t);
private:
	// technical data:

	int updateInterval;
	int nGenerations;

	int TEMP;
	bool useDistr;
	bool avoidBO;

	double forgetRate;
	double precision;
	double MAX_ITER;

	double ResidualLife;
	double Energy;
	double Capacity;
	double DeltaDischarge;
	double DeltaCharge;
	
	double ThresholdES;
	double StdDevES;

	double STARTEnergy;
	double STARTResidualLife;
	// decisional data LONG
	int presentLife;
	double Threshold;
	double StdDev;
	double performanceThreshold;
	double minThreshold;
	double FromNonStorage;

	// HistoricalData:
	std::vector<TvBatteryState> BatteryHistory; // qua memorizzo la storia della Batteria
	std::vector<systemState>  SystemStates; // qua memorizzo la storia della Rete 
	std::vector<double> ThresholdEvolution;
	std::vector<double> StdDevEvolution;

	std::vector<double> ThresholdESEvolution;
	std::vector<double> StdDevESEvolution;

	double sign(double x) {
		if (x > 0) 
			return 1;
		if (x < 0)
			return -1;
		else
			return 0;
	}

};

#endif
