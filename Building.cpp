// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#include "eg.h"
#include "Building.h"
#include <fstream>
#include <math.h>

using namespace std;

// methods to interact with the world
double Building::Ask(systemState s, bool forced){
	return currentNeed;
}

double Building::Bid(systemState s, bool forced){
	return 0.0;
}

void Building::Transfer(systemState s, double amount){
	//cout << amount << endl;
	historyNeed.push_back(amount);
	updateNeed();
}



// method to change internal state
void Building::updateNeed() {
	
    static random_device r;
    static mt19937 gen{r()};
    //gen.seed(time(NULL) + this->sequence);
    normal_distribution<> d(0,1); // normal distribution with mean 0 and standard deviation 1
	
	// complex:
	
	// PER GRAFICO THRESHOLD VS TIME SWITCH
	//if (historyNeed.size() < 24 * 50) {
	//	double temp1 = amplitude*(0.50 + 0.50*(double)rand() / (RAND_MAX))*exp(-pow((historyNeed.size() % 24) - 8.0 + 2.0*(double)rand() / (RAND_MAX)-1.0, 2.0) / 2.0) +
	//		amplitude*(1.0 + 0.05*(2.0*(double)rand() / (RAND_MAX)-1))*exp(-pow((historyNeed.size() % 24) - 20.0 + 2.0*(double)rand() / (RAND_MAX)-1.0, 2.0) / 3.0) +
	//		noiseVariance*d(gen);
	//	this->currentNeed = max(0.4, temp1);
	//}
	//else {
	//	double temp1 = 170.0*(0.50 + 0.50*(double)rand() / (RAND_MAX))*exp(-pow((historyNeed.size() % 24) - 8.0 + 2.0*(double)rand() / (RAND_MAX)-1.0, 2.0) / 2.0) +
	//		170.0*(1.0 + 0.05*(2.0*(double)rand() / (RAND_MAX)-1))*exp(-pow((historyNeed.size() % 24) - 20.0 + 2.0*(double)rand() / (RAND_MAX)-1.0, 2.0) / 3.0) +
	//		50.0*d(gen);
	//	this->currentNeed = max(0.4, temp1);
	//}
	
	// Normale:
	double temp1 = amplitude*(0.50 + 0.50*(double)rand() / (RAND_MAX))*exp(-pow((historyNeed.size() % 24) - 8.0 + 2.0*((double)rand() / (RAND_MAX))-1.0, 2.0) / 2.0) +
		amplitude*(1.0 + 0.05*(2.0*(double)rand() / (RAND_MAX)-1.0))*exp(-pow((historyNeed.size() % 24) - 20.0 + 2.0*((double)rand() / (RAND_MAX))-1.0, 2.0) / 3.0) +
		noiseVariance*d(gen);
	double temp = max(0.4,temp1);
	this->currentNeed = temp;


	//if (historyNeed.size()%(12) ==0 && historyNeed.size()>0) {
	//	this->currentNeed = 200;
	//}else {
	//	this->currentNeed = 0.0;
	//}


	//if (historyNeed.size()%((int)period) ==0 && historyNeed.size()>0) {
	//	this->currentNeed = amplitude;
	//}else {
	//	this->currentNeed = 0.0;
	//}
	

	//if ((historyNeed.size()%24) % 13 == 0 && (historyNeed.size()%24)>0) {
	//	this->currentNeed = 200;// 900;//200;
	//}else if ((historyNeed.size() % 24) % 17 == 0 && (historyNeed.size()%24)>0) {
	//	this->currentNeed = 1050;// 600;//1050;
	//}else{
	//	this->currentNeed = 0.0;
	//}

	
}


void Building::setPeriodicSwitch(int SwitchPeriod, double NewAmplitude, double NewVariance) {
	this->switchAskPeriodic = true;
	this->switchTime = SwitchPeriod;
	this->newAmplitude = NewAmplitude;
	this->newVariance = NewVariance;
	this->switchPeriod = 0;
}

void Building::setSwitch(int SwithTime, double NewAmplitude, double NewVariance){
	this->switchAsk = true;
	this->switchTime = SwithTime;
	this->newAmplitude = NewAmplitude;
	this->newVariance = NewVariance;
	LOG << "cambio in t= " << this->switchTime << " ampl: " << this->newAmplitude << " variance: " << this->newVariance;
}

// plot
void Building::DumpHtml(string path) {
	ostringstream oss;
	oss << path << "/building" << Id << ".html";
	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;

	// drawHistoryAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;

	for (int i = 0; i < historyNeed.size(); i++) {
		fileO << "      [" << i << "," << historyNeed[i] << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div id=\"BidAsk\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

void Building::startSimulation(){
	while (historyNeed.size() != 0) {
		historyNeed.erase(historyNeed.begin());
	}
	return;
}

void Building::getForecast(double** scenariosMatrix, int nScenarios, int timeHorizonForecast){

	static random_device r;
	static mt19937 gen{ r() };
	//gen.seed(time(NULL) + this->sequence);
	normal_distribution<> d(0, 1); // normal distribution with mean 0 and standard deviation 1

	for (int i = 0; i < nScenarios; i++) {
		for (int t = 0; t < timeHorizonForecast; t++) {
			scenariosMatrix[i][t] -= abs(max(amplitude*sin(2 * pi*(historyNeed.size()+t) / period), 0.0) + noiseVariance*d(gen));
		}
	}
}

bool Building::isStorage() {
	return false;
}

