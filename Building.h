// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_BUILDING_H
#define EG_BUILDING_H

#include <random>

#include "Player.h"

using namespace std;

class Building : public Player {
public:
	Building(int s) {
		this->sequence = s;
		updateNeed();
	}

	Building(double period, double amplitude, double noiseVariance) {
		this->pi = atan(1) * 4;
		this->period = period;
		this->amplitude = amplitude;
		this->noiseVariance = noiseVariance;
		this->switchAskPeriodic = false;
		this->switchAsk = false;
		updateNeed();
	}


	virtual bool isStorage();

	virtual double Ask(systemState s,bool forced = false);
	virtual double Bid(systemState s, bool forced = false);
	
	virtual void Transfer(systemState s, double amount);
	virtual void DumpHtml(string path);
	virtual void endSimulation() { return; };
	virtual void startSimulation();

	void getForecast(double** scenariosMatrix, int nScenarios, int timeHorizonForecast);

	void setPeriodicSwitch(int SwithPeriod, double NewAmplitude, double NewVariance);
	void setSwitch(int SwithTime, double NewAmplitude, double NewVariance);

private:
    int sequence;
    double currentNeed;
    void updateNeed();
	vector<double> historyNeed;

	double period;
	double amplitude;
	double noiseVariance;
	double pi;

	int switchPeriod;
	bool switchAskPeriodic;
	bool switchAsk;
	double switchTime;
	double newAmplitude;
	double newVariance;
	
};

#endif //EG_Building_H
