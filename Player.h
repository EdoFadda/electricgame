// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_PLAYER_H
#define EG_PLAYER_H
#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>

using namespace std;

struct systemState {
	bool utopy;
	bool normal;
	bool danger;
	bool blackOut;
	double totAsk;
	double totBid;
	double totAskForced;
	double totBidForced;
};



class Player {
public:
	Player() {
		this->Payoff = 0.0;
		this->X = 0.0;
		this->Y = 0.0;
	}

	virtual bool isStorage() = 0;
    
	virtual std::string Tag();

    virtual double Ask(systemState s, bool forced = false) = 0;
    virtual double Bid(systemState s, bool forced = false) = 0;

    // Perform the actual energy transfer (positive: the player gets the energy)
    virtual void Transfer(systemState s, double amount) = 0;

	virtual void endSimulation() { return; };
	virtual void startSimulation() { return; };

	// plot results
	virtual void DumpHtml(string path) = 0;
	

    // Payoff functions
    double GetPayoff();
	int getId() {
		return Id;
	}
	void Punishment();

	

	// Geography
	void setPosition(double X, double Y) {
		this->X = X;
		this->Y = Y;
	}
    
	double getX(){
		return X;
	}

	double getY() {
		return Y;
	}
protected:
	int Id;
    double Payoff;
	double X;
	double Y;

};

#endif //EG_PLAYER_H
