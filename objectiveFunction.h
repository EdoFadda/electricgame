#ifndef OBJECTIVEFUNCTION_H
#define OBJECTIVEFUNCTION_H


#include <iostream>
#include <iomanip>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <cmath>

using namespace std;

class objectiveFunction{
public:
	objectiveFunction() {};
	objectiveFunction(double** scenariosMatrix, int lengthW, int nScenarios, int timeHorizonScenarios, double energy0, double deltaCharge, double deltaDischarge, double capacity);
	// nei dati finali magari metti solo il solo il alla batteria
	double compute(double* weights);

private:
	double** scenariosMatrix;
	int lengthW;
	int nScenarios;
	int timeHorizonScenarios;
	double energy0; 
	double deltaCharge;
	double deltaDischarge; 
	double capacity;
};

#endif