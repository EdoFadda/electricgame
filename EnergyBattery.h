// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_ENERGYBATTERY_H
#define EG_ENERGYBATTERY_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <random>
#include "string"
#include "Player.h"
#include "Battery.h"



struct EnergyBatteryState {
	double ResidualLife;
	double Energy;
	double Transfert;
	double Ask;
	double Bid;
	double AskReal;
	double BidReal;
	double PayOff;
};

class EnergyBattery : public Battery {
public:

	EnergyBattery(double cap, double energy0, int life, double DeltaCharge,double DeltaDischarge, double varianceChangeThreshold,double threshold = 0.0) : Battery(cap, energy0, life) {
		this->DeltaCharge = DeltaCharge;
		this->DeltaDischarge = DeltaDischarge;
		this->Energy = energy0;
		this->minEnergy = energy0;
		if (energy0 == 0) {
			LOG << "Please set energy0 to be higher, otherwise the deterministic evolution of the threshold will suffer.";
		}
		this->Capacity = cap;
		this->ResidualLife = life;
		this->Threshold = threshold;
		this->Payoff = 0.0;
		this->presentStat = { 0,0.0,0.0 };
		this->bestStat = { -1, 0.0, 0.0};
		if (varianceChangeThreshold <= 0.0) {
			this->varianceChangeThreshold = cap / 10.0;
		}else{
			this->varianceChangeThreshold = varianceChangeThreshold;
		}
		this->presentLife = 0;
		BatteryHistory.push_back({ResidualLife, Energy, 0.0,0.0,0.0,0.0,Payoff});
	}

	bool isStorage();

	double Ask(systemState S, bool forced = false);
	double Bid(systemState S, bool forced = false);
	void Transfer(systemState s, double amount);


	void newThreshold(int version, bool verbose = false, int nVite=0); // comando da usare quando finisco un set di vite
	void setVarianceChangeThreshold(double value);
	
	
	void startSimulation();
	void endSimulation();
	

	void printThreshold();

	void DumpHtml(string path);
	
	void printStatistics();
	void changeThresholdTo(double newThreshold);

	double getEnergy();
	double getEnergyThreshold();
	double getDeltaCharge();
	double getResidualLife();

	double getMeanPayOff();
	double Brain(bool forced);
	
private:
	// technical data:
	double ResidualLife;
	double Energy;
	double Load;
	double Capacity;
	double DeltaDischarge;
	double DeltaCharge;

	// decisional data LONG
	int presentLife;
	double Threshold;
	double varianceChangeThreshold;
	double BestEnergyThreshold;
	double minEnergy;

	//double QuantileThreshold = 1.281552; // 0.9 quantile
	//double QuantileThreshold = 0.8416212; // 0.8 quantile
	double QuantileThreshold = 0.5244005; // 0.7 quantile

	// decision data short SHORT
	double ask;
	double bid;

	// statistics of past lives(with different coefficients):
	statistics bestStat;

	// statistics of lives:
	statistics presentStat;


	// HistoricalData:
	std::vector<EnergyBatteryState> BatteryHistory; // qua merizzo la storia della Batteria nella sua vita corrente
	std::vector<systemState>  SystemStates;


	double sign(double x) {
		if (x > 0) 
			return 1;
		if (x < 0)
			return -1;
		else
			return 0;
	}

};

#endif //EG_BATTERY_H
