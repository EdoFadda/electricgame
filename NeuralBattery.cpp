// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include<fstream>
#include "eg.h"
#include "Player.h"
#include "Battery.h"
#include "Building.h"
#include "NuclearPlant.h"
#include "RenewablePlant.h"
#include "EnergyBattery.h"
#include "NeuralBattery.h"
#include "objectiveFunction.h"
#include "swarm.h"

using namespace std;


# define CHARGE_CODE  1
# define DISCHARGE_CODE  -1


// methods to interact with the world
double NeuralBattery::Ask(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}

	double bf = Brain(s,forced);
	if (bf > 0) {
		ask = bf;
		bid = 0.0;
		return bf;
	}
	else {
		return 0.0;
	}
	


}

double NeuralBattery::Bid(systemState s, bool forced) {
	if (ResidualLife <= 0) {
		ask = 0.0;
		bid = 0.0;
		return 0.0;
	}
	double bf = Brain(s,forced);
	if (bf < 0) {
		ask = 0.0;
		bid = -bf;
		return -bf;
	}
	else {
		return 0.0;
	}

	

}

void NeuralBattery::Transfer(systemState s, double amount) {
	// amount negativo -> ceduto energia
	// amount positivo -> acqistato energia
	if (ResidualLife == 0) {
		NeuralBatteryState bS = { ResidualLife, Energy, 0.0, 0.0 ,0.0 ,0.0, 0.0, Payoff };
		BatteryHistory.push_back(bS);
		return;
	}

	Energy = min(Energy + amount, Capacity);
	SystemStates.push_back(s);

	// Arrange life
	
	if (BatteryHistory.size() == 0 ){
		ResidualLife--;
	}else{
		//if (sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert) ) // se non hanno lo stesso segno allora
		//	ResidualLife--;
		if (amount > 0 && sign(amount) != sign(BatteryHistory[BatteryHistory.size() - 1].Transfert))
			ResidualLife -=  tanh(Energy) + 1.0;
	}
	
	
	// update BatteryHistory
	NeuralBatteryState bS = {ResidualLife, Energy, amount, ask ,bid ,amount, 0.0, Payoff};
	BatteryHistory.push_back(bS);
	this->presentLife++;
}


// output methods
double NeuralBattery::getEnergy() {
	return Energy;
}


double NeuralBattery::getDeltaCharge(){
	return this->DeltaCharge;
}



// internal intelligence
double NeuralBattery::Brain(systemState s, bool forced) {
	if (forced) {
		return -min(DeltaDischarge, Energy);
	}
	else {
		if (BatteryHistory[BatteryHistory.size() - 1].Transfert > 0) { // se all'istante prima mi stavo caricando continuo a farlo.
			return min(DeltaDischarge, Capacity);
		}

		// prendo la decisione
		int N = memory + 1;
		double result = weight[0]*s.totBid;

		for (int i = 1; i < memory; i++) {
			result += weight[i]*SystemStates[i].totBid;
		}
		result += this->Energy*weight[N - 1] + weight[N];
		if (result > 0.0 ) {
			return min(DeltaDischarge, Capacity);
		}
		else {
			return 0.0;
		}
	}
}



void NeuralBattery::computeWeights() {
	
	int nScenarios = 10;

	// inizializzo gli scenari
	double** scenariosMatrix = new double*[nScenarios];
	for (int i = 0; i < nScenarios; i++) {
		scenariosMatrix[i] = new double[timeHorizonForecast];
		for (int t = 0; t < timeHorizonForecast; t++)
			scenariosMatrix[i][t] = 0.0;
	}

	generaScenari(nScenarios, scenariosMatrix);
	

	objectiveFunction* obj = new objectiveFunction(scenariosMatrix,memory-1+2,nScenarios,timeHorizonForecast,Energy,DeltaCharge,DeltaDischarge,Capacity);


	srand((unsigned)time(0));
	int MAX_INPUTS = 3;
	int nParticles = 10;
	double* START_RANGE_MIN = new double[memory - 1 + 2];
	double* START_RANGE_MAX = new double[memory - 1 + 2];
	for (int i = 0; i < memory - 1 + 2; i++) {
		START_RANGE_MIN[i] = 0.0;
		START_RANGE_MAX[i] = 1.0;
	}
	START_RANGE_MIN[memory - 1 + 1] = 0;
	START_RANGE_MAX[memory - 1 + 1] = Capacity;

	int nEpochs = 20;

	swarm* s = new swarm(MAX_INPUTS, nParticles, nEpochs, 100.0, START_RANGE_MIN, START_RANGE_MAX, obj, 1.0, 2.0, 2.0);
	s->psoAlgorithm();
	





}



void  NeuralBattery::generaScenari(int nScenarios, double** scenariosMatrix){

	// creo i vector per la descrizione delle altre batterie
	vector<double> thresholds;
	vector<double> energies;
	vector<double> deltaCharge;

	// acquisisco i dati:
	for (auto p : neighbors) {
		if ((dynamic_cast<Building*>(p) != NULL)) {
			((Building*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
		}
		else if ((dynamic_cast<EnergyBattery*>(p) != NULL)) {
			thresholds.push_back(((EnergyBattery*)p)->getEnergy());
			energies.push_back(((EnergyBattery*)p)->getEnergyThreshold());
			deltaCharge.push_back(((EnergyBattery*)p)->getDeltaCharge());
		}
		else if ((dynamic_cast<NuclearPlant*>(p) != NULL)) {
			((NuclearPlant*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
		}
		else if ((dynamic_cast<RenewablePlant*>(p) != NULL)) {
			((RenewablePlant*)p)->getForecast(scenariosMatrix, nScenarios, timeHorizonForecast);
		}
	}

	// simulo il comportamento delle EnergyBatteries
	for (int i = 0; i < nScenarios; i++) {
		simulateBatteries(scenariosMatrix[i], timeHorizonForecast, thresholds, energies, deltaCharge, false);
	}

}



bool  NeuralBattery::isStorage() {
	return true;
}

// plot
void NeuralBattery::DumpHtml(string path) {
	ostringstream oss;
	oss << path << "/battery" << Id << ".html";

	ofstream fileO(oss.str().c_str());

	fileO << "<html>" << endl;
	fileO << "<head>" << endl;
	fileO << "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>" << endl;

	fileO << " <script type=\"text/javascript\">" << endl;
	fileO << "google.load('visualization', '1', {packages: ['corechart', 'line']});" << endl;

	// chiamo le funzioni per visualizzare i grafici
	fileO << "google.setOnLoadCallback(drawEnergy);" << endl;
	fileO << "google.setOnLoadCallback(drawBidAsk);" << endl;
	fileO << "google.setOnLoadCallback(drawResidualLife);" << endl;

	// drawEnergy
	fileO << "function drawEnergy() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'energy ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Energy << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('Energy'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	// drawBidAsk
	fileO << "function drawBidAsk() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'bid ');" << endl;
	fileO << " data.addColumn('number', 'ask ');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].Bid << "," << BatteryHistory[i].Ask << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('BidAsk'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;


	// drawResidualLife
	fileO << "function drawResidualLife() {" << endl;
	fileO << " var data = new google.visualization.DataTable();" << endl;
	fileO << " data.addColumn('number', 'X');" << endl;
	fileO << " data.addColumn('number', 'residualLife');" << endl;
	fileO << " data.addRows([" << endl;
	for (int i = 0; i < BatteryHistory.size(); i++) {
		fileO << "      [" << i << "," << BatteryHistory[i].ResidualLife << "]," << endl;
	}
	fileO << "  ]);" << endl;
	fileO << "  var options = {" << endl;
	fileO << "    hAxis: {" << endl;
	fileO << "     title: 'Time'" << endl;
	fileO << "   }," << endl;
	fileO << "    vAxis: {" << endl;
	fileO << "       title: ' y ' " << endl;
	fileO << "    }" << endl;
	fileO << "   };" << endl;
	fileO << "  var chart = new google.visualization.LineChart(document.getElementById('ResidualLife'));" << endl;
	fileO << "   chart.draw(data, options);" << endl;
	fileO << " }" << endl;

	fileO << "  </script>" << endl;
	// Corpo HTML
	fileO << "</head>" << endl;
	fileO << "<body>" << endl;
	// Scenario
	fileO << "		<div>BatteryId: " << Id << " ResidualLife: " << ResidualLife << " payoff: " << Payoff << "</div>" << endl;
	fileO << "		<div id=\"BidAsk\"></div>" << endl;
	fileO << "		<div id=\"Energy\"></div>" << endl;
	fileO << "		<div id=\"ResidualLife\"></div>" << endl;

	fileO << "</body>" << endl;
	fileO << "</html>" << endl;
	fileO.close();
}

double NeuralBattery::getMeanPayOff() {
	double meanPayOff = presentStat.sumPayoff / presentStat.ntimeStep;
	return meanPayOff;
}