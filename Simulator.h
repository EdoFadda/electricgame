// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.

#ifndef EG_SIMULATOR_H
#define EG_SIMULATOR_H

#include <list>
#include "eg.h"

#include "Player.h"

using namespace std;

# define DETEVO 0
# define STOCEVO 1
# define SIMULATEDANEALING 2

class Simulator {
private:
    vector<Player *> players;

	vector<systemState> historyStates;

	double stdProduction;

	
public:
	Simulator(vector<Player *>&  p);
	
	Simulator() {}


	int firstPowerOutage();
	bool happensBlackOut();

	bool isStorage(Player* p) {
		return false;
	};


    void Run(int T, bool verbose =  false);

	void UpdateBatteriesThreshold(int version, bool verbose = false, int nVite = 0);
	
	void generateGeography(double minX, double maxX, double minY, double maxY, double percBorder);	
	
	void dumpResult(string path, int count=-1);

	void dumpGeograpy(string path);
	
	void plotXYGraph(vector<double>& x, vector<double>& y, string path, int count = 0);
	
	void plotXYGraphTXT(vector<double>& x, vector<double>& y, string path);

	void trainBatteries(int nViteTraining, int durataVite,bool verbose);


	double maxAsk();
};

#endif //EG_SIMULATOR_H
