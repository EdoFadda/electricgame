// This file is part of ElectricGames
// Copyright © 2016 Edoardo Fadda & Giovanni Squillero
//
// ElectricGames is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ElectricGames is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ElectricGames.  If not, see <http://www.gnu.org/licenses/>.


#ifndef EG_RENEWABLEPLANT_H
#define EG_RENEWABLEPLANT_H

#include "eg.h"
#include "Player.h"

class RenewablePlant : public Player {
public:
    virtual std::string Tag() {
        return fmt::format("Renewable plant({0})", reinterpret_cast<const void *>(this));
    }

	virtual bool isStorage();

	virtual double Ask(systemState s, bool forced = false);
	virtual double Bid(systemState s, bool forced = false);


	virtual void DumpHtml(string path);
	virtual void startSimulation() { return; };
	virtual void endSimulation() { return; };
	virtual void Transfer(systemState s, double amount);

	void getForecast(double** scenariosMatrix, int nScenarios, int timeHorizonForecast);

private:
	vector<double> historyProduction;
};

#endif //EG_NUCLEARPLANT_H
